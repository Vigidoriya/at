package hw16.maksimava.PageObjects;

import org.openqa.selenium.By;

public class EbayProductPage extends Page {
    private final By addToCardButton = By.xpath("//a[@id='isCartBtn_btn']");
    private final By productName = By.id("itemTitle");


    public EbayCartPage addProductToTheCart() {
        driver.findElement(addToCardButton).click();
        return new EbayCartPage();
    }

    public String getProductName() {
        return driver.findElement(productName).getText();
    }
}
