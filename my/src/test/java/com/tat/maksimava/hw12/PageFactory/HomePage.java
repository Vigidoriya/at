package com.tat.maksimava.hw12.PageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

class HomePage extends Page {

    @FindBy(xpath = "//*[@*='enter']")
    private WebElement enterAccountButton;
    @FindBy(xpath = "//*[(@*='login')][@*='text']")
    private WebElement loginField;
    @FindBy(xpath = "//*[(@*='password')]")
    private WebElement passwordField;
    @FindBy(xpath = "//*[@*='b-hold']/*[@*='submit']")
    private WebElement submitButton;

    HomePage(WebDriver driver) {
        super(driver);
    }

    boolean checkLogout() {
        return (enterAccountButton.getText().equals("Войти"));
    }

    HomePage openAuthorizationForm() {
        enterAccountButton.click();
        return new HomePage(getDriver());
    }

    HomeUserPage enterAccount() {
        submitButton.click();
        return new HomeUserPage(getDriver());
    }

    HomePage fillRegistrationForm(String login, String password) {
        loginField.sendKeys(login);
        passwordField.sendKeys(password);
        return new HomePage(getDriver());
    }

}
