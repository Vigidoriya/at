package hw18.maksimava.Services;

import hw18.maksimava.PageObjects.EbayHomePage;
import hw18.maksimava.Utils.Highlighting;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HomePageServices {
    private static Logger log = (Logger) LogManager.getLogger(ProductPageServices.class);
    public static void searchProduct(String query) {
        EbayHomePage homePage = new EbayHomePage();
        homePage.enterQuery(query);
        homePage.clickSearch();
        log.debug("Search query "+query);
    }

    public static void openEbayHomePage() {
        new EbayHomePage().open();
    }
}
