package com.tat.maksimava.hw10;

import org.testng.annotations.Factory;

public class FactoryTest {
    @Factory(dataProvider = "radParams", dataProviderClass = TestParams.class)
    public Object[] createTest(double a) {
        return new Object[]{new TrigonometricFunctionsTest(a)};
    }
}
