package hw18.maksimava.PageObjects;

import hw18.maksimava.Utils.Highlighting;
import hw18.maksimava.Utils.Screenshot;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class EbayProductPage extends Page {
    private final By addToCardButton = By.xpath("//a[@id='isCartBtn_btn']");
    private final By productName = By.id("itemTitle");

    public EbayCartPage addProductToTheCart() {
        WebElement addingToTheCart = driver.findElement(addToCardButton);
        log.trace("Find button 'add to the cart'");
        new Highlighting().highlightElement(addingToTheCart);
        new Screenshot().getScreenshot();
        addingToTheCart.click();
        log.trace("Adding product to the cart");
        return new EbayCartPage();
    }

    public String getProductName() {
        WebElement product = driver.findElement(productName);
        new Highlighting().highlightElement(product);
        return product.getText();
    }
}
