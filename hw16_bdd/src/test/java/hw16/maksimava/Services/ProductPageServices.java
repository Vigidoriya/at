package hw16.maksimava.Services;

import hw16.maksimava.PageObjects.EbayProductPage;

public class ProductPageServices {
    public static String getProductName() {
        return new EbayProductPage().getProductName();
    }

    public static void addToCart() {
        new EbayProductPage().addProductToTheCart();
    }
}
