package hw16.maksimava.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import hw16.maksimava.Services.ProductPageServices;
import org.testng.Assert;

import static hw16.maksimava.Services.CartPageServices.getMessage;
import static hw16.maksimava.Services.CartPageServices.getPageTitle;
import static hw16.maksimava.Services.CartPageServices.getTextCart;
import static hw16.maksimava.Services.ProductPageServices.addToCart;
import static hw16.maksimava.Services.ResultsPageService.openFirstProductResult;

public class EbayAddToCartDef {
    String productName;

    @When("^I click the first link in the results grid on the Ebay Results Page$")
    public void openTheFirstLinkInTheResultsGrid() {
        openFirstProductResult();
    }

    @When("^I get product name in the product description label on the Ebay Product Page$")
    public void getProductNameInTheProductDescription() {
        productName = ProductPageServices.getProductName();
    }

    @When("^I click the button 'Add to cart' in the product description on the Ebay Product Page$")
    public void addToTheCart() {
        addToCart();
    }

    @Then("^The cart page should be opened$")
    public void verifyOpenCartPage() {
        Assert.assertEquals(getPageTitle(), "Ваша корзина");
    }

    @And("^The term query \"([^\"]*)\" should be contains in the cart grid on the Ebay Cart Page$")
    public void theProductNameShouldBeInTheCartGridOnTheEbayCartPage(String query) {
        Assert.assertTrue(getTextCart().contains(query));
    }

    @And("^The product name should be in the message field on the Ebay Cart Page$")
    public void theProductNameShouldBeInTheMessageFieldOnTheEbayCartPage() {
        Assert.assertEquals(getMessage(), productName + " был добавлен в вашу корзину.");
    }
}