package com.tat.maksimava.hw14.TUTBY.Tests;

import com.tat.maksimava.hw14.TUTBY.Services.HomePageService;
import com.tat.maksimava.hw14.TUTBY.Services.HomeUserPageService;
import com.tat.maksimava.hw14.TUTBY.Services.MailPageService;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TUTBYTest extends ConfigurationTest {

    @Test
    public void testTutBy() {
        String messageContent;
        String accountName;

        accountName = new HomePageService().logInMailAndGetAccountName(account);
        Assert.assertEquals(accountName, account.getUsername(), "User name is " + accountName);

        String pageTitle = new HomeUserPageService().enterMailAndGetPageTitle();
        Assert.assertTrue(pageTitle.contains("Почта"), "Page title isn't 'Почта'");

        MailPageService mailService = new MailPageService();
        mailService.createDraft(mail);
        messageContent = mailService.sendDraftAndGetDraftContent(mail.getSubject());
        Assert.assertTrue(messageContent.contains(mail.getAddress()), messageContent + " doesn't contains '" + mail.getAddress() + "' in Drafts");
        Assert.assertTrue(messageContent.contains(mail.getText()), messageContent + " doesn't contains '" + mail.getText() + "' in Drafts");

        messageContent = mailService.findMailInDraftContent(mail.getSubject());
        Assert.assertNull(messageContent, "Mail with subject '" + mail.getSubject() + "' contains in Drafts");

        messageContent = mailService.findMailInSentContent(mail.getSubject());
        Assert.assertTrue(messageContent.contains(mail.getAddress()), messageContent + " doesn't contains '" + mail.getAddress() + "' in Sent");
        Assert.assertTrue(messageContent.contains(mail.getText()), messageContent + " doesn't contains '" + mail.getText() + "' in Sent");

        accountName = new MailPageService().logOutMailAndGetAccountName();
        Assert.assertNotEquals(accountName, account.getUsername(), "Unsuccessful logOutAndGetAccountName");
    }
}
