package com.tat.maksimava.hw14.TUTBY.BO;

public class Mail {
    private String address;
    private String subject;
    private String text;

    Mail(String address, String subject, String text) {
        this.address = address;
        this.subject = subject;
        this.text = text;
    }

    public String getAddress() {
        return address;
    }

    public String getSubject() {
        return subject;
    }

    public String getText() {
        return text;
    }
}
