package hw4.com.tat.maksimava.Strategy;

import java.util.Arrays;

import static java.lang.Integer.parseInt;

public class Strategy {

    public static void main(String[] args) {
        int parametr;

        try {
            Integer.parseInt(args[0]);
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("Enter arguments: '1' for bubble sort, '2' for selection sort");
            return;
        } catch (NumberFormatException ex) {
            System.out.println("Enter arguments: '1' for bubble sort, '2' for selection sort");
            return;
        }
        parametr = parseInt(args[0]);

        SortingContext sortContext = new SortingContext();
        switch (parametr) {
            case 1: {
                sortContext.setStrategy(new BubbleSort());
                break;
            }
            case 2: {
                sortContext.setStrategy(new SelectionSort());
                break;
            }
            default: {
                System.out.println("Enter arguments: '1' for bubble sort, '2' for selection sort");
                return;
            }
        }
        int[] array = new int[10];
        for (int i = 0; i < 10; i++) {
            array[i] = (int) (Math.random() * 100);
        }
        System.out.println("Unsorted array \n" + Arrays.toString(array));

        sortContext.execute(array);
        System.out.println("Sorted array \n" + Arrays.toString(array));
    }
}




