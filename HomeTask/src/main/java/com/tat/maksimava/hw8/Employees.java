package com.tat.maksimava.hw8;

import java.util.ArrayList;
import java.util.List;

public class Employees {
    public List<Employee> employeesTable = new ArrayList<>();

    public void addEmployee(String firstName, String lastName) {
        for (int i = 0; i < employeesTable.size(); i++) {
            if ((employeesTable.get(i).getFirstName().equals(firstName)) && (employeesTable.get(i).getLastName().equals(lastName))) {
                System.out.println("Employee " + firstName + " " + lastName + " already exists");
                return;
            }
        }
        employeesTable.add(new Employee(getId(), firstName, lastName));
        System.out.println("Employee " + firstName + " " + lastName + "  added");
    }

    private int getId() {
        int id;
        if (employeesTable.size() >= 1) id = employeesTable.get(employeesTable.size() - 1).getId() + 1;
        else id = 1;
        return id;
    }

    public void removeEmployeeById(int id) {
        int del = 0;
        for (int i = 0; i < employeesTable.size(); i++) {
            if (employeesTable.get(i).getId() == id) {
                employeesTable.remove(i);
                System.out.println("Employee with id " + id + " removed");
                del++;
            }
        }
        if (del == 0) System.out.println("Employee doesn't exist");
    }

    public void showEmployees() {
        for (int i = 0; i < employeesTable.size(); i++) {
            System.out.print(employeesTable.get(i).getId() + " ");
            System.out.print(employeesTable.get(i).getFirstName() + " ");
            System.out.println(employeesTable.get(i).getLastName());
        }
    }

    public void setEmployees(int id, String firstName, String lastName) {
        employeesTable.add(new Employee(id, firstName, lastName));
    }
}
