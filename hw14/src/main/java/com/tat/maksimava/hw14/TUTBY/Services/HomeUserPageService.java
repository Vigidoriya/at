package com.tat.maksimava.hw14.TUTBY.Services;

import com.tat.maksimava.hw14.TUTBY.PageObject.HomeUserPage;

public class HomeUserPageService {

    public String enterMailAndGetPageTitle() {
        return new HomeUserPage().openMailPage().getTitle();
    }
}
