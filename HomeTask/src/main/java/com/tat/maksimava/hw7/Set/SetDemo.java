package com.tat.maksimava.hw7.Set;

import java.util.*;

public class SetDemo {
    public static void main(String[] args) {
        HashSet<Integer> setInteger1 = new HashSet<Integer>();
        Collections.addAll(setInteger1, 1, 2, 3, 8);
        HashSet<Integer> setInteger2 = new HashSet<Integer>();
        Collections.addAll(setInteger2, 8, 3, 6, 7);

        HashSet<Double> setDouble1 = new HashSet<Double>();
        Collections.addAll(setDouble1, 1.3, 2.5, 3.7, 4.9);
        HashSet<Double> setDouble2 = new HashSet<Double>();
        Collections.addAll(setDouble2, 4.9, 3.7, 6.4, 7.2);

        HashSet<String> setString1 = new HashSet<String>();
        Collections.addAll(setString1, "a", "c", "d");
        HashSet<String> setString2 = new HashSet<String>();
        Collections.addAll(setString2, "b", "a", "d");


        Set s1 = new Set(setInteger1, setInteger2);
        System.out.println("Integer Set1: " + setInteger1.toString());
        System.out.println("Integer Set2: " + setInteger2.toString());
        System.out.println("Union: " + s1.unions().toString());
        System.out.println("Intersections: " + s1.intersections().toString());
        System.out.println("Complement Set1/Set2: " + s1.complement().toString());
        System.out.println("Symmetric difference: " + s1.symmetricDifference().toString());

        Set s2 = new Set(setDouble1, setDouble2);
        System.out.println("\nDouble Set1: " + setDouble1.toString());
        System.out.println("Double Set2: " + setDouble2.toString());
        System.out.println("Union: " + s2.unions().toString());
        System.out.println("Intersections: " + s2.intersections().toString());
        System.out.println("Complement Set1/Set2: " + s2.complement().toString());
        System.out.println("Symmetric difference: " + s2.symmetricDifference().toString());

        Set s3 = new Set(setString1, setString2);
        System.out.println("\nString Set1: " + setString1.toString());
        System.out.println("String Set2: " + setString2.toString());
        System.out.println("Union: " + s3.unions().toString());
        System.out.println("Intersections: " + s3.intersections().toString());
        System.out.println("Complement Set1/Set2: " + s3.complement().toString());
        System.out.println("Symmetric difference: " + s3.symmetricDifference().toString());
    }
}
