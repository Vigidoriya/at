package hw3.com.tat.maksimava;

import java.util.Arrays;

class Median {

    public static double median(double[] array) {
        int mid;
        double mediana;
        Arrays.sort(array);

        if (array.length % 2 == 0) {
            mid = (array.length) / 2;
            mediana = (array[mid - 1] + array[mid]) / 2;
        } else {
            mid = (array.length) / 2;
            mediana = array[mid];
        }
        return mediana;
    }

    public static float median(int[] array) {
        int mid;
        float mediana;
        Arrays.sort(array);
        if (array.length % 2 == 0) {
            mid = (array.length) / 2;
            mediana = (float) (array[mid - 1] + array[mid]) / 2;
        } else {
            mid = (array.length) / 2;
            mediana = (float) array[mid];
        }
        return mediana;
    }
}


