package com.tat.maksimava.hw6;

public class ArrayWrapper<T> {
    T[] arr;

    ArrayWrapper(T[] mas) {
        arr = (T[]) new Object[mas.length + 1];
        System.arraycopy(mas, 0, arr, 1, mas.length);
    }

    T get(int n) {
        if ((n <= 0) || (n >= arr.length)) {
            throw new IncorrectArrayWrapperIndex("Incorrect index");
        } else {
            return arr[n];
        }
    }

    boolean replace(Integer index, T change) {
        get(index);
        if (change instanceof String) {
            if (((String) change).length() > ((String) arr[index]).length()) {
                arr[index] = change;
                return true;
            }
        } else if (change instanceof Integer) {
            if (((Integer) change) > (Integer) arr[index]) {
                arr[index] = change;
                return true;
            }
        } else {
            arr[index] = change;
        }
        return false;
    }
}

