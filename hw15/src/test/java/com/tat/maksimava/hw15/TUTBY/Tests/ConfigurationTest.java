package com.tat.maksimava.hw15.TUTBY.Tests;

import com.tat.maksimava.hw15.TUTBY.BO.Account;
import com.tat.maksimava.hw15.TUTBY.BO.AccountFactory;
import com.tat.maksimava.hw15.TUTBY.BO.MailCreator.MailAddressCreator;
import com.tat.maksimava.hw15.TUTBY.BO.MailCreator.MailCreator;
import com.tat.maksimava.hw15.TUTBY.BO.MailCreator.SubjectCreator;
import com.tat.maksimava.hw15.TUTBY.BO.MailCreator.TextCreator;
import com.tat.maksimava.hw15.TUTBY.Driver.SingletonWebDriver;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class ConfigurationTest {
    private static final String MAIL_URL = "https://www.tut.by/";
    Account account;
    private MailCreator textCreator = new TextCreator(), subjectCreator = new SubjectCreator(), addressCreator = new MailAddressCreator();
    String text, subject, address;

    @BeforeClass
    public void setUp() {
        WebDriver driver = SingletonWebDriver.getWebDriverInstance();
        driver.get(MAIL_URL);
        account = AccountFactory.getAccount();
        text = textCreator.create();
        subject = subjectCreator.create();
        address = addressCreator.create();
    }

    @AfterClass
    public void reset() {
        SingletonWebDriver.close();
    }
}
