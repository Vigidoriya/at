@ebay
Feature: Search product

  Scenario:Running a Full Text Quick Search

    Given I opened Ebay Home Page
    When I search the product "asus k55vd" on the Ebay Home Page
    Then the term query "asus k55vd" should be contained in the links in the results grid on the first EbayResultsPage

  Scenario Outline: Running a Full Text Quick Search

    Given I opened Ebay Home Page
    When I search the product "<request>" on the Ebay Home Page
    Then the term query "<request>" should be contained in the links in the results grid on the first EbayResultsPage

    Examples:
      | request       |
      | playstation 4 |
      | xbox one      |