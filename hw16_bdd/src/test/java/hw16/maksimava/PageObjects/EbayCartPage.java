package hw16.maksimava.PageObjects;

import org.openqa.selenium.By;

public class EbayCartPage extends Page {

    private final By cartContent = By.xpath("//*[@id='ShopCart']/descendant::a[@data-mtdes]");
    private final By message = By.xpath("//div[@class='msgWrapper']//span");

    public String getCartContent() {
        return driver.findElement(cartContent).getText();
    }

    public String getPageTitle() {
        return driver.getTitle();
    }

    public String getMessage() {
        return driver.findElement(message).getText();
    }
}