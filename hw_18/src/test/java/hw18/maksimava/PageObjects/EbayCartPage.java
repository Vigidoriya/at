package hw18.maksimava.PageObjects;

import hw18.maksimava.Utils.Highlighting;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class EbayCartPage extends Page {

    private final By cartContent = By.xpath("//*[@id='ShopCart']/descendant::a[@data-mtdes]");
    private final By message = By.xpath("//div[@class='msgWrapper']//span");

    public String getCartContent() {
        WebElement content = driver.findElement(cartContent);
        new Highlighting().highlightElement(content);
        return content.getText();
    }

    public String getPageTitle() {
        return driver.getTitle();
    }

    public String getMessage() {
        WebElement messageField = driver.findElement(message);
        new Highlighting().highlightElement(messageField);
        return messageField.getText();
    }

}
