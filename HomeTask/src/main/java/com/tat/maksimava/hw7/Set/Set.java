package com.tat.maksimava.hw7.Set;

import java.util.HashSet;
import java.util.Iterator;

public class Set<T> {
    HashSet<T> set1, set2, setResult;

    Set(HashSet<T> set1, HashSet<T> set2) {
        this.set1 = set1;
        this.set2 = set2;
    }

    public HashSet<T> unions() {
        setResult = new HashSet<T>();
        setResult.addAll(set1);
        setResult.addAll(set2);
        return setResult;
    }

    public HashSet<T> intersections() {
        setResult = new HashSet<T>();
        setResult.addAll(set1);
        setResult.retainAll(set2);
        return setResult;
    }

    public HashSet<T> complement() {
        setResult = new HashSet<T>();
        setResult.addAll(set1);
        setResult.removeAll(set2);
        return setResult;
    }

    public HashSet<T> symmetricDifference() {
        setResult = new HashSet<T>();
        setResult.addAll(set1);
        setResult.addAll(set2);
        Iterator<T> itr = set1.iterator();
        while (itr.hasNext()) {
            T element = itr.next();
            if (set2.contains(element)) setResult.remove(element);
        }
        return setResult;
    }
}
