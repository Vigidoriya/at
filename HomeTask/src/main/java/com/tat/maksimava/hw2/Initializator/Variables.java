package com.tat.maksimava.hw2.Initializator;

public class Variables {
    static boolean bool;
    static char c;
    static byte b;
    static short s;
    static int i;
    static long l;
    static float f;
    static double d;

    static String str;
    static int[] arr;
    static Boolean bool1;
    static Character c1;
    static Byte b1;
    static Short s1;
    static Integer i1;
    static Long l1;
    static Float f1;
    static Double d1;

    public static void primitiveTypes() {
        System.out.println("Primitive types:");
        System.out.println("boolean=" + bool + ";");
        System.out.println("char=" + c + ";");
        System.out.println("byte=" + b + ";");
        System.out.println("short=" + s + ";");
        System.out.println("int=" + i + ";");
        System.out.println("long=" + l + ";");
        System.out.println("float=" + f + ";");
        System.out.println("double=" + d + ";");
    }

    public static void referenceTypes() {
        System.out.println("\nReference types:");
        System.out.println("String=" + str + ";");
        System.out.println("Array=" + arr + ";");
        System.out.println("Boolean=" + bool1 + ";");
        System.out.println("Character=" + c1 + ";");
        System.out.println("Byte=" + b1 + ";");
        System.out.println("Short=" + s1 + ";");
        System.out.println("Integer=" + i1 + ";");
        System.out.println("Long=" + l1 + ";");
        System.out.println("Float=" + f1 + ";");
        System.out.println("Double=" + d1 + ";");
    }

}

