package hw4.com.tat.maksimava.Strategy;

public class SelectionSort implements Sorter {
    public void sort(int[] array) {
        int min, temp;

        for (int i = 0; i < array.length - 1; i++) {
            min = i;
            for (int next = i + 1; next < array.length; next++) {
                if (array[next] < array[min]) {
                    min = next;
                }
            }
            temp = array[min];
            array[min] = array[i];
            array[i] = temp;
        }
    }
}

