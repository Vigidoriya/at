package com.tat.maksimava.hw14.TUTBY.SelenideServices;

import com.tat.maksimava.hw14.TUTBY.BO.Account;
import com.tat.maksimava.hw14.TUTBY.SelenidePageObject.HomePage;

public class HomePageService {
    public String logInMailAndGetAccountName(Account account) {
        return new HomePage().openAuthorizationForm().fillRegistrationForm(account).enterAccount().getAccountName();
    }

    public String getAccountName() {
        return new HomePage().getAccountName();
    }
}
