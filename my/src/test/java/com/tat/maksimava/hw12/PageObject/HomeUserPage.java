package com.tat.maksimava.hw12.PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


class HomeUserPage extends Page {
    private final By accountMenuButton = By.xpath("//*[@*='uname']");
    private final By mailLink = By.xpath("//*[contains(@*,'mail.tut.by')]");
    private final By exitAccountButton = By.xpath("//*[contains(@*,'tut.by/logout')]");

    HomeUserPage(WebDriver driver) {
        super(driver);
    }

    HomeUserPage accountMenu() {
        getDriver().findElement(accountMenuButton).click();
        return new HomeUserPage(getDriver());
    }

    MailPage openMailPage() {
        getDriver().findElement(mailLink).click();
        return new MailPage(getDriver());
    }

    HomePage exitAccount() {
        getDriver().findElement(exitAccountButton).click();
        return new HomePage(getDriver());
    }

    String getAccountName() {
        return getDriver().findElement(accountMenuButton).getText();
    }
}
