package webServices;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class httpClientTest {
    private final String url = "http://jsonplaceholder.typicode.com/posts";

    @Test
    public void verifyGetStatusCode() throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);
        Assert.assertEquals(response.getStatusLine().getStatusCode(), 200);
        ((CloseableHttpClient) client).close();
    }

    @Test
    public void verifyGetResponseHeader() throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);
        Assert.assertEquals(response.getFirstHeader("Content-Type").getValue(), "application/json; charset=utf-8");
        ((CloseableHttpClient) client).close();
    }

    @Test
    public void verifyGetResponseBody() throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);

        String result = EntityUtils.toString(response.getEntity());
        JSONArray jsonArray = new JSONArray(result);
        Assert.assertEquals(jsonArray.length(), 100);
        ((CloseableHttpClient) client).close();
    }

    @Test
    public void testCreateNewPost() throws Exception {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(url);
        String json = "data: {'title': 'foo', 'body': 'bar','userId': 1}";

        StringEntity entity = new StringEntity(json);
        post.setEntity(entity);

        HttpResponse response = client.execute(post);
        Assert.assertEquals(response.getStatusLine().getStatusCode(), 201);

        String result = EntityUtils.toString(response.getEntity());
        Assert.assertTrue(result.length() > 0);

        JSONObject jsonObject = new JSONObject(result);
        System.out.println(jsonObject.get("id"));
        ((CloseableHttpClient) client).close();
    }

    @Test
    public void testUpdatePost() throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPut putRequest = new HttpPut("http://jsonplaceholder.typicode.com/posts/1");
        String json = "data: { 'id': 1, 'title': 'foo', 'body': 'bar','userId': 1}";
        putRequest.setEntity(new StringEntity(json));
        HttpResponse response = client.execute(putRequest);
        Assert.assertEquals(response.getStatusLine().getStatusCode(), 200);
        String result = EntityUtils.toString(response.getEntity());
        JSONObject jsonObject = new JSONObject(result);
        Assert.assertEquals(jsonObject.get("id"), 1);
        ((CloseableHttpClient) client).close();
    }

    @Test
    public void testDeletePost() throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpDelete deleteRequest = new HttpDelete("http://jsonplaceholder.typicode.com/posts/1");
        HttpResponse response = client.execute(deleteRequest);
        Assert.assertEquals(response.getStatusLine().getStatusCode(), 200);
        ((CloseableHttpClient) client).close();
    }
}
