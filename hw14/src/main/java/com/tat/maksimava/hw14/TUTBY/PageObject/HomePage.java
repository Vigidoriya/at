package com.tat.maksimava.hw14.TUTBY.PageObject;

import com.tat.maksimava.hw14.TUTBY.BO.Account;
import org.openqa.selenium.By;

public class HomePage extends Page {

    private final By enterAccountButton = By.xpath("//*[@*='enter']");
    private final By loginField = By.xpath("//*[(@*='login')][@*='text']");
    private final By passwordField = By.xpath("//*[(@*='password')]");
    private final By submitButton = By.xpath("//*[@*='b-hold']/*[@*='submit']");

    public HomePage openAuthorizationForm() {
        driver.findElement(enterAccountButton).click();
        return new HomePage();
    }

    public HomeUserPage enterAccount() {
        driver.findElement(submitButton).click();
        return new HomeUserPage();
    }

    public String getAccountName() {
        return driver.findElement(enterAccountButton).getText();
    }

    public HomePage fillRegistrationForm(Account account) {
        driver.findElement(loginField).sendKeys(account.getLogin());
        driver.findElement(passwordField).sendKeys(account.getPassword());
        return new HomePage();
    }
}