package com.tat.maksimava.hw15.TUTBY.SelenideServices;

import com.tat.maksimava.hw15.TUTBY.BO.Account;
import com.tat.maksimava.hw15.TUTBY.SelenidePageObject.HomePage;

public class HomePageService {
    public String logInMailAndGetAccountName(Account account) {
        return new HomePage().openAuthorizationForm().fillRegistrationForm(account).enterAccount().getAccountName();
    }

    public String getAccountName() {
        return new HomePage().getAccountName();
    }
}
