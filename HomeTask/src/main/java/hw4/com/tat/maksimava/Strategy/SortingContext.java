package hw4.com.tat.maksimava.Strategy;

public class SortingContext {
    private Sorter sorter;

    public void setStrategy(Sorter sorter) {
        this.sorter = sorter;
    }

    public void execute(int[] array) {
        sorter.sort(array);
    }
}
