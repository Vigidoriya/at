package com.tat.maksimava.hw14.TUTBY.SelenidePageObject;

import static com.codeborne.selenide.Selenide.*;

import org.openqa.selenium.By;

public class HomeUserPage {

    private final By accountMenuButton = By.xpath("//*[@*='uname']");
    private final By exitAccountButton = By.xpath("//*[contains(@*,'tut.by/logout')]");

    public String getAccountName() {
        return $(accountMenuButton).getText();
    }

    public HomeUserPage accountMenu() {
        $(accountMenuButton).click();
        return page(HomeUserPage.class);
    }

    public HomePage exitAccount() {
        $(exitAccountButton).click();
        return page(HomePage.class);
    }
}
