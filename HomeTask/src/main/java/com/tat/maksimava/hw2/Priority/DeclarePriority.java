package com.tat.maksimava.hw2.Priority;

public class DeclarePriority {
    private static String staticVariable1;
    private static String staticVariable2;
    private String var1;
    private String var2;
    String var3;

    DeclarePriority() {
        this.var3 = "Initialization in constructor";
        System.out.println("Default constructor: " + var3);
    }

    DeclarePriority(String str) {
        this.var3 = "Variable from constructor ";
        System.out.println("Variable from constructor with parameter: " + str);
    }

    public static String getStaticVariable1() {
        System.out.println("Getter static variable1: " + staticVariable1);
        return staticVariable1;
    }

    public static String getStaticVariable2() {
        System.out.println("Getter static variable2: " + staticVariable2);
        return staticVariable2;
    }

    public void setStaticVariable1(String staticVariable1) {
        this.staticVariable1 = staticVariable1;
    }

    public void setStaticVariable2(String staticVariable2) {
        this.staticVariable2 = staticVariable2;
    }

    public String getVar1() {
        System.out.println("Getter var1: " + var1);
        return var1;
    }

    public void setVar1(String var1) {
        this.var1 = var1;
    }

    public String getVar2() {
        System.out.println("Getter var2: " + var2);
        return var2;
    }

    public void setVar2(String var2) {
        this.var2 = var2;

    }

    {
        System.out.println(" Initialization block1");
        setVar1("Class variable1");
        System.out.println(getVar1());
    }

    {
        System.out.println(" Initialization block2");
        setVar2("Class variable2");
        System.out.println(getVar2());
    }

    static {
        System.out.println("Static initialization block1");
        staticVariable1 = "Static variable1";
    }

    static {
        System.out.println("Static initialization block2");
        staticVariable2 = "Static variable2";
    }


}
