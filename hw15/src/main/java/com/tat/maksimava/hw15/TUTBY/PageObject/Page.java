package com.tat.maksimava.hw15.TUTBY.PageObject;

import com.tat.maksimava.hw15.TUTBY.Driver.SingletonWebDriver;
import com.tat.maksimava.hw15.TUTBY.Utils.Waits;
import org.openqa.selenium.WebDriver;

public class Page {
    WebDriver driver = SingletonWebDriver.getWebDriverInstance();
    Waits waits = new Waits();

    public String getTitle() {
        return driver.getTitle();
    }
}
