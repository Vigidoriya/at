package com.tat.maksimava.hw10;

import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.Arrays;
import java.util.List;

public class TestCustomRunner {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        List<String> files = Arrays.asList(
                "testng.xml",
                "TestArithmetic.xml",
                "TestTrigonometric.xml"
        );

        testNG.setTestSuites(files);
        testNG.setParallel(XmlSuite.ParallelMode.METHODS);
        testNG.setThreadCount(3);
        testNG.run();
    }
}


