package com.tat.maksimava.hw14.TUTBY.PageObject;

import com.tat.maksimava.hw14.TUTBY.Browser.Browser;
import com.tat.maksimava.hw14.TUTBY.Utils.Waits;
import org.openqa.selenium.WebDriver;

public class Page {
    WebDriver driver = Browser.getWebDriverInstance();
    Waits waits = new Waits();

    public String getTitle() {
        return driver.getTitle();
    }
}
