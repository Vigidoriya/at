import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import hw16.maksimava.Browser.Browser;
import org.testng.annotations.AfterClass;


@CucumberOptions(
        strict = true,
        plugin = {
                "pretty", "json:target/Cucumber.json",
                "html:target/cucumber-html-report"
        },
        tags = {"@ebay"}
)
public class EbayTest extends AbstractTestNGCucumberTests {
    @AfterClass
    public void reset() {
        Browser.close();
    }


}