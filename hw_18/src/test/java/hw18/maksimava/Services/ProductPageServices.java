package hw18.maksimava.Services;

import hw18.maksimava.PageObjects.EbayProductPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ProductPageServices {
    private static Logger log = (Logger) LogManager.getLogger(ProductPageServices.class);

    public static String getProductName() {
        log.debug("Product name: " + new EbayProductPage().getProductName());
        return new EbayProductPage().getProductName();
    }

    public static void addToCart() {
        new EbayProductPage().addProductToTheCart();
    }

}
