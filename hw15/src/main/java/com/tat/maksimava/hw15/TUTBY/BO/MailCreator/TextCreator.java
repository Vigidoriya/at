package com.tat.maksimava.hw15.TUTBY.BO.MailCreator;

public class TextCreator implements MailCreator {
    @Override
    public String create() {
        return "Design Patterns " + System.currentTimeMillis();
    }
}
