package com.tat.maksimava.hw7.Sorting;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SortingDemo {
    public static void main(String[] args) {
        List<Integer> array = Arrays.asList(131, 656, 232, 100, 0, 565, 11, 1000, 33, 131, 856, 1111, 0);
        System.out.println("Before sorting:");
        for (Integer itr : array) {
            System.out.print(itr + " ");
        }
        Collections.sort(array, new Sorting());
        System.out.println("\nAfter sorting:");
        for (Integer itr : array) {
            System.out.print(itr + " ");
        }
    }
}
