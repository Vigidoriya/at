package hw18.maksimava.PageObjects;

import hw18.maksimava.Utils.Highlighting;
import hw18.maksimava.Utils.Screenshot;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class EbayHomePage extends Page {
    private static final String BASE_URL = "https://www.ebay.com";
    private final By searchField = By.xpath("//input[@type='text']");
    private final By searchButton = By.xpath("//input[@type='submit']");

    public EbayHomePage open() {
        driver.get(BASE_URL);
        return this;
    }

    public EbayHomePage enterQuery(String query) {
        wait.waitForElementVisibility(searchField).sendKeys(query);
        return new EbayHomePage();
    }

    public EbayResultsPage clickSearch() {
        WebElement search = wait.waitForElementVisibility(searchButton);
        new Highlighting().highlightElement(search);
        new Screenshot().getScreenshot();
        search.click();
        return new EbayResultsPage();
    }

}
