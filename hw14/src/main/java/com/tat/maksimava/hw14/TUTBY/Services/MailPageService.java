package com.tat.maksimava.hw14.TUTBY.Services;

import com.tat.maksimava.hw14.TUTBY.BO.Mail;
import com.tat.maksimava.hw14.TUTBY.PageObject.MailPage;


public class MailPageService {

    public MailPage createDraft(Mail mail) {
        return new MailPage().createNewMail().fillMailForm(mail).saveMailAsADraft();
    }

    public String sendDraftAndGetDraftContent(String subject) {
        String content = findMailInDraftContent(subject);
        new MailPage().sendMail();
        return content;
    }

    public String findMailInDraftContent(String subject) {
        if (new MailPage().openDrafts().isMailExists(subject)) return new MailPage().getDraftContentMessage();
        else return null;
    }

    public String findMailInSentContent(String subject) {
        if (new MailPage().openSent().isMailExists(subject)) {
            return new MailPage().getSentContentMessage();
        } else return null;
    }

    public String logOutMailAndGetAccountName() {
        return new MailPage().openHomeUserPage().accountMenu().exitAccount().getAccountName();
    }


}
