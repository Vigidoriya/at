package hw1.com.tat.maksimava;
public class CalculationG {
    static int a, p;
    static double m1, m2;

    public static void main(String[] args) {
        double G;
        if (args.length == 4) {
            try {
                a = Integer.parseInt(args[0]);
                p = Integer.parseInt(args[1]);
                m1 = Double.parseDouble(args[2]);
                m2 = Double.parseDouble(args[3]);
            } catch (NumberFormatException e) {
                System.out.println("Please, enter the correct values: " + e);
                return;
            }
            G = 4 * Math.pow(Math.PI, 2) * Math.pow(a, 3) / (Math.pow(p, 2) * (m1 + m2));
            System.out.println("G=" + G);
        } else {
            System.out.println("Please, enter the correct number of arguments!");
        }
    }
}

