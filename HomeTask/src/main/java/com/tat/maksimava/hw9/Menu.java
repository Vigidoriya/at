package com.tat.maksimava.hw9;

import java.util.ArrayList;
import java.util.List;

public class Menu {
    List menu;

    public Menu() {
        setMenu();
    }

    public List setMenu() {
        menu = new ArrayList();
        menu.add("\n1. Add employee");
        menu.add("2. Remove employee by ID");
        menu.add("3. Show employees table");
        menu.add("4. Exit");
        return menu;
    }

    public void showMenu() {
        for (Object s : menu) {
            System.out.println(s);
        }
    }

    public void getMenu(String i) {
        Employee employee = new Employee();
        switch (i) {
            case "1": {
                employee.addEmployee();

                break;
            }
            case "2": {
                employee.removeEmployeeById();
                break;
            }
            case "3": {
                employee.showEmployee();
                break;
            }
            case "4": {
                System.exit(0);

            }
            default: {
                System.out.println("Choose '1-4'");
                break;
            }
        }
    }
}