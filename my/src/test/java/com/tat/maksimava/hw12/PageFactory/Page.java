package com.tat.maksimava.hw12.PageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class Page {
    private final WebDriver driver;
    private int timeToWaitTitle = 5;

    public Page(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    void waitForTitleContains(String title) {
        new WebDriverWait(driver, timeToWaitTitle).until(ExpectedConditions.titleContains(title));
    }
}
