package com.tat.maksimava.hw10;

import org.testng.annotations.DataProvider;

public class TestParams {
    @DataProvider(name = "longSumParams")
    public static Object[][] getlongSumParameters() {
        return new Object[][]{
                {-10, 77, -10 + 77},
                {-20, 0, -20},
                {Long.MAX_VALUE, Long.MAX_VALUE, Long.MAX_VALUE + Long.MAX_VALUE},
                {-100, Long.MIN_VALUE, -100 + Long.MIN_VALUE}
        };
    }

    @DataProvider(name = "doubleSumParams")
    public static Object[][] getDoubleSumParameters() {
        return new Object[][]{
                {2.43, 2.11, 4.54},
                {-4.8, 0, -4.8},
                {Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE + Double.MAX_VALUE},
                {-4.70, Double.MIN_VALUE, -4.70 + Double.MIN_VALUE}
        };
    }

    @DataProvider(name = "longSubParams")
    public static Object[][] getlongSubParameters() {
        return new Object[][]{
                {-10, -77, 67},
                {-5, 0, -5},
                {Long.MIN_VALUE, Long.MAX_VALUE, Long.MIN_VALUE - Long.MAX_VALUE}
        };
    }

    @DataProvider(name = "doubleSubParams")
    public static Object[][] getDoubleSubParameters() {
        return new Object[][]{
                {4.5, -5.5, 10.0},
                {-4.8, 0, -4.8},
                {Double.MIN_VALUE, Double.MAX_VALUE, Double.MIN_VALUE - Double.MAX_VALUE}
        };
    }

    @DataProvider(name = "longMultParams")
    public static Object[][] getLongMultParameters() {
        return new Object[][]{
                {3, 0, 0},
                {-6, -5, 30},
                {Long.MAX_VALUE, Long.MAX_VALUE, Long.MAX_VALUE * Long.MAX_VALUE}
        };
    }

    @DataProvider(name = "doubleMultParams")
    public static Object[][] getDoubleMultParameters() {
        return new Object[][]{
                {5.3, 0, 0},
                {-3.2, -6.5, -3.2 * -6.5},
                {Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE * Double.MAX_VALUE}
        };
    }

    @DataProvider(name = "divLongParams")
    public static Object[][] getDivLongParameters() {
        return new Object[][]{
                {-70, 5, -70 / 5},
                {-60, -9, -60 / -9},
                {Long.MIN_VALUE, Long.MAX_VALUE, Long.MIN_VALUE / Long.MAX_VALUE}
        };
    }

    @DataProvider(name = "divDoubleParams")
    public static Object[][] getDivDoubleParameters() {
        return new Object[][]{
                {-67.0, -6.3, -67.0 / -6.3},
                {44.4, 11.1, 44.4 / 11.1},
                {Double.MIN_VALUE, Double.MAX_VALUE, Double.MIN_VALUE / Double.MAX_VALUE}
        };
    }

    @DataProvider(name = "powParams")
    public static Object[][] getPowParameters() {
        return new Object[][]{
                {4.0, 0.5, Math.pow(4.0, 0.5)},
                {8.1, -9, Math.pow(8.1, -9)},
                {0.0, 44.0, Math.pow(0.0, 44.0)},
                {1, 32, Math.pow(1, 32)}
        };
    }

    @DataProvider(name = "sqrtParams")
    public static Object[][] getSqrtParameters() {
        return new Object[][]{
                {81, Math.sqrt(81)},
                {67.5, Math.sqrt(67.5)},
                {-25, Math.sqrt(-25)}
        };
    }

    @DataProvider(name = "positiveTestParams")
    public static Object[][] getPositiveTestParameters() {
        return new Object[][]{
                {0, false},
                {-77, false},
                {54, true}
        };
    }

    @DataProvider(name = "negativeTestParams")
    public static Object[][] getNegativeTestParameters() {
        return new Object[][]{
                {0, false},
                {-77, true},
                {54, false}
        };
    }

    @DataProvider(name = "radParams")
    public static Object[][] getradParameters() {
        return new Object[][]{
                {0},
                {Math.toRadians(30)},
                {Math.toRadians(45)},
                {Math.toRadians(60)},
                {Math.toRadians(90)},
                {Math.toRadians(180)},
                {Math.toRadians(270)},
                {Math.toRadians(360),},
                {Math.toRadians(-30)}

        };
    }
}
