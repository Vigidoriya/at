package com.tat.maksimava.hw9;

import java.sql.*;
import java.util.Scanner;

import static com.tat.maksimava.hw9.ConnectionSql.*;

public class Employee {


    public void showEmployee() {
        try {
            ResultSet resultSet = getStatement().executeQuery("select * from Employee");
            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String firstname = resultSet.getString("firstname");
                String lastname = resultSet.getString("lastname");
                System.out.println(id + " " + firstname + " " + lastname);
            }
            resultSet.close();
            getStatement().close();
            closeConnection();
        } catch (Exception ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
            System.exit(0);
        }
    }

    public void addEmployee() {
        Scanner in = new Scanner(System.in);
        String firstName = "", lastName = "";
        System.out.println("Enter first name or 'q' to back in menu ");
        while (firstName.isEmpty()) firstName = in.nextLine();
        if (firstName.equals("q")) return;
        System.out.println("Enter last name or 'q' to back in menu");
        while (lastName.isEmpty()) lastName = in.nextLine();
        if (lastName.equals("q")) return;
        try {
            ResultSet employees = getStatement().executeQuery("SELECT firstname,lastname FROM Employee;");
            while (employees.next()) {
                if (employees.getString("firstname").equals(firstName) &&
                        (employees.getString("lastname").equals(lastName))) {
                    System.out.println("Employee " + firstName + " " + lastName + " already exists");
                    employees.close();
                    return;
                }
            }
            ResultSet maxId = getStatement().executeQuery("SELECT MAX(id) FROM Employee;");
            int count = maxId.getInt(1);
            maxId.close();

            PreparedStatement statementAdd = getConnection().prepareStatement("insert into Employee(id,firstname,lastname) values(?,?,?)");
            statementAdd.setString(1, Integer.toString(++count));
            statementAdd.setString(2, firstName);
            statementAdd.setString(3, lastName);

            int counter = statementAdd.executeUpdate();
            if (counter != 0) System.out.println("Employee " + firstName + " " + lastName + " added");
            statementAdd.close();
            closeConnection();

        } catch (Exception ex) {
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
            System.exit(0);
        }
    }

    public void removeEmployeeById() {
        Scanner in = new Scanner(System.in);
        String removeEmployeeId = "";
        System.out.println("Enter ID employee to remove or 'q' to back in menu");
        while (removeEmployeeId.isEmpty()) removeEmployeeId = in.nextLine();
        try {
            Integer.parseInt(removeEmployeeId);
        } catch (NumberFormatException ex) {
            System.out.println("ID can take only integers");
            return;
        }
        if (removeEmployeeId.equals("q")) return;
        if (Integer.parseInt(removeEmployeeId) < 0) {
            System.out.println("ID can't be negative");
            return;
        }
        try {
            PreparedStatement statementRemove = getConnection().prepareStatement(/*"" + */"DELETE from Employee where id=?");
            statementRemove.setString(1, removeEmployeeId);
            int executeCounter = statementRemove.executeUpdate();
            if (executeCounter == 0) System.out.println("ID don't found");
            else System.out.println("Employee with ID " + removeEmployeeId + " removed");
            statementRemove.close();
            closeConnection();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }
}
