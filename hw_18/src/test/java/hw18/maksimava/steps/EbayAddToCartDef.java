package hw18.maksimava.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import hw18.maksimava.Services.ProductPageServices;
import hw18.maksimava.Utils.Screenshot;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import static hw18.maksimava.Services.CartServices.*;
import static hw18.maksimava.Services.ProductPageServices.addToCart;
import static hw18.maksimava.Services.ResultsPageService.openFirstProductResult;

public class EbayAddToCartDef {
    private static Logger log = (Logger) LogManager.getLogger(EbayAddToCartDef.class);
    String productName;

    @When("^I click the first link in the results grid on the Ebay Results Page$")
    public void openTheFirstLinkInTheResultsGrid() {
        openFirstProductResult();
        new Screenshot().getScreenshot();
    }

    @When("^I get product name in the product description label on the Ebay Product Page$")
    public void getProductNameInTheProductDescription() {
        productName = ProductPageServices.getProductName();
    }

    @When("^I click the button 'Add to cart' in the product description on the Ebay Product Page$")
    public void addToTheCart() {
        try {
            addToCart();
        } catch (Exception ex) {
            ex.printStackTrace();
            log.fatal("Impossible add product to the cart");
            throw new Error("Impossible add product to the cart");
        }
        new Screenshot().getScreenshot();
    }

    @Then("^The cart page should be opened$")
    public void verifyOpenCartPage() {
        Assert.assertEquals(getPageTitle(), "Ваша корзина");
        log.trace("Opened cart page");
        new Screenshot().getScreenshot();
    }

    @And("^The term query \"([^\"]*)\" should be contains in the cart grid on the Ebay Cart Page$")
    public void theProductNameShouldBeInTheCartGridOnTheEbayCartPage(String query) {
        Assert.assertTrue(getTextCart().contains(query));
        log.info("Product " + query + " exists in the cart");
        new Screenshot().getScreenshot();
    }

    @And("^The product name should be in the message field on the Ebay Cart Page$")
    public void theProductNameShouldBeInTheMessageFieldOnTheEbayCartPage() {
        try {
            Assert.assertEquals(getMessage(), productName + " был добавлен в вашу корзину.");
        } catch (AssertionError error) {
            log.error("Message field is: " + getMessage());
        }
        new Screenshot().getScreenshot();
    }
}
