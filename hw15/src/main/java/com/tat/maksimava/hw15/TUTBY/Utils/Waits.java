package com.tat.maksimava.hw15.TUTBY.Utils;

import com.tat.maksimava.hw15.TUTBY.Driver.SingletonWebDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waits {
    private WebDriver driver = SingletonWebDriver.getWebDriverInstance();
    private int timeToWaitTitle = 5;

    public void waitForTitleContains(String title) {
        new WebDriverWait(driver, timeToWaitTitle).until(ExpectedConditions.titleContains(title));
        ;
    }
}
