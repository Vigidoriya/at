package com.tat.maksimava.hw2.Priority;

public class Priority {
    public static void main(String[] args) {
        {
            DeclarePriority dp1, dp2;
            System.out.println("Create reference variable");
            dp1 = new DeclarePriority("dp1");
            dp2 = new DeclarePriority();
            dp1.getVar1();
            dp2.getVar2();
            dp1.setVar1("Setter var1");
            dp2.setVar2("Setter var2");
            dp1.getVar1();
            dp2.getVar2();
            System.out.println("Assignment of a reference to an object");
            DeclarePriority.getStaticVariable1();
            DeclarePriority.getStaticVariable2();
            dp1.setStaticVariable1("Setter for static variable 1");
            dp1.setStaticVariable2("Setter for static variable 2");
            DeclarePriority.getStaticVariable1();
            DeclarePriority.getStaticVariable2();
        }

    }

}

