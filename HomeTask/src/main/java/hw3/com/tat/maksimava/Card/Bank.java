package hw3.com.tat.maksimava.Card;

public class Bank {
    public static String clientsCards[][] = new String[30][2];
    double sum = 0;
    double balance = 0;
    private String name;


    public static void createNewCard(String name, double balance) {
        if (balance < 0) throw new IllegalArgumentException("Impossible to create card with negative sum");
        for (int i = 0; i < clientsCards.length; i++) {
            if (clientsCards[i][0] == name) {
                System.out.println("Account " + name + " already exists");
                return;
            }
        }
        for (int i = 0; i < clientsCards.length; i++) {
            if (clientsCards[i][0] == null) {
                clientsCards[i][0] = name;
                clientsCards[i][1] = Double.toString(balance);
                System.out.println("Card " + name + " created");
                return;
            }
        }
    }

    public double getCardBalance(Card card) {
        name = card.getName();
        for (int i = 0; i < clientsCards.length; i++) {
            if (clientsCards[i][0] == name) {
                balance = Double.valueOf(clientsCards[i][1]);
                break;
            }
        }
        System.out.println("Balance " + name + " is " + balance);
        return balance;
    }

    public void addBalance(Card card, double sum) {
        if (sum <= 0) throw new IllegalArgumentException("Invalid parameter: the sum can not be a negative number");
        this.sum = sum;
        name = card.getName();
        for (int i = 0; i < clientsCards.length; i++) {
            if (clientsCards[i][0] == name) {
                sum = Double.parseDouble(clientsCards[i][1]) + sum;
                clientsCards[i][1] = Double.toString(sum);
                break;
            }
        }
        System.out.println(name + " balance increased by " + sum);
    }

    public void lessBalance(Card card, double sum) {
        if (sum <= 0) throw new IllegalArgumentException("Invalid parameter: the sum can not be a negative number");
        this.sum = sum;
        name = card.getName();
        for (int i = 0; i < clientsCards.length; i++) {
            if (clientsCards[i][0] == name) {
                balance = Double.parseDouble(clientsCards[i][1]);
                if (balance >= sum) {
                    balance -= sum;
                    System.out.println(name + " balance decreased by " + sum);
                } else {
                    System.out.println("There are not enough resources on the account " + name);
                }
                clientsCards[i][1] = Double.toString(balance);
                break;
            }
        }
    }

    public double convert(Card card, double convert) {
        name = card.getName();
        for (int i = 0; i < clientsCards.length; i++) {
            if (clientsCards[i][0] == name) {
                sum = Double.parseDouble(clientsCards[i][1]) * convert;
                break;
            }
        }
        System.out.println("The converted " + name + " balance is " + sum);
        return sum;
    }
}
