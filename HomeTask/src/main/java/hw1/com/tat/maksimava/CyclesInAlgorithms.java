package hw1.com.tat.maksimava;

public class CyclesInAlgorithms {
    static int algorithmId, loopType, n;

    public static void main(String[] args) {
        int factorial = 0;
        String s = "";
        if (args.length == 3) {
            try {
                algorithmId = Integer.parseInt(args[0]);
                n = Integer.parseInt(args[2]);
                loopType = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                System.out.println("Please, enter the integer values of arguments: " + e);
            }

            switch (algorithmId) {
                case 1: {
                    Fibonacci f = new Fibonacci();
                    switch (loopType) {
                        case 1:
                            s = f.loopTypeWhile(n);
                            break;
                        case 2:
                            s = f.loopTypeDoWhile(n);
                            break;
                        case 3:
                            s = f.loopTypeFor(n);
                            break;
                        default:
                            System.out.println("In the second parametr only values '1','2','3' can be selected ");
                            return;
                    }
                    System.out.println(s);
                    break;
                }
                case 2: {
                    Factorial f = new Factorial();
                    switch (loopType) {
                        case 1:
                            factorial = f.loopTypeWhile(n);
                            break;
                        case 2:
                            factorial = f.loopTypeDoWhile(n);
                            break;
                        case 3:
                            factorial = f.loopTypeFor(n);
                            break;
                        default:
                            System.out.println("In the second parametr only values '1','2','3' can be selected ");
                            return;
                    }
                    System.out.println("Factorial of " + n + " is equal " + factorial);
                    break;
                }
                default:
                    System.out.println("In the first parametr only values '1' and '2' can be selected ");
            }
        } else {
            System.out.println("Please, enter the correct number of arguments!");
        }
    }
}

class Fibonacci {
    String s = "Fibonacci numbers:0 1 ";
    int[] f;

    String loopTypeFor(int p) {
        f = new int[p];
        for (int i = 2; i < p; i++) {
            f[0] = 0;
            f[1] = 1;
            f[i] = f[i - 1] + f[i - 2];
            s += f[i] + " ";
        }
        return s;
    }

    String loopTypeWhile(int p) {
        f = new int[p];
        int i = 2;
        while (i < p) {
            f[0] = 0;
            f[1] = 1;
            f[i] = f[i - 1] + f[i - 2];
            s += f[i] + " ";
            i++;
        }
        return s;
    }

    String loopTypeDoWhile(int p) {
        f = new int[p];
        int i = 2;
        do {
            f[0] = 0;
            f[1] = 1;
            f[i] = f[i - 1] + f[i - 2];
            s += f[i] + " ";
            i++;
        } while (i < p);
        return s;
    }
}

class Factorial {
    int fact = 1, i = 1;

    int loopTypeDoWhile(int p) {
        do {
            fact *= i;
            i++;
        } while (i <= p);
        return fact;
    }

    int loopTypeWhile(int p) {
        while (i <= p) {
            fact *= i;
            i++;
        }
        return fact;
    }

    int loopTypeFor(int p) {
        for (i = 1; i <= p; i++) {
            fact *= i;
        }
        return fact;
    }
}