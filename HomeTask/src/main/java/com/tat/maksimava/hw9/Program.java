package com.tat.maksimava.hw9;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {

        Menu menu = new Menu();
        Scanner in = new Scanner(System.in);
        for (; ; ) {
            menu.showMenu();
            String choice = in.nextLine();
            menu.getMenu(choice);
        }
    }
}
