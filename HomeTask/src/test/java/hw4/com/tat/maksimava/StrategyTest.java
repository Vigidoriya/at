package hw4.com.tat.maksimava;

import hw4.com.tat.maksimava.Strategy.BubbleSort;
import hw4.com.tat.maksimava.Strategy.SelectionSort;
import hw4.com.tat.maksimava.Strategy.SortingContext;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class StrategyTest {
    SortingContext sortContext = new SortingContext();

    @Test
    public void testSrtaregyBubbleSort() {
        int[] array = {9, 6, 4, 9, 5, 9, 4, 3, 0, 1};
        int[] expect = {0, 1, 3, 4, 4, 5, 6, 9, 9, 9};
        sortContext.setStrategy(new BubbleSort());
        sortContext.execute(array);
        Assert.assertEquals(Arrays.toString(array), Arrays.toString(expect));
    }

    @Test
    public void testStrategySelectionSort() {
        int[] array = {7, 6, 2, 9, 5, 10, 4, 3, 6, 0};
        int[] expect = {0, 2, 3, 4, 5, 6, 6, 7, 9, 10};
        sortContext.setStrategy(new SelectionSort());
        sortContext.execute(array);
        Assert.assertEquals(Arrays.toString(array), Arrays.toString(expect));

    }
}
