package com.tat.maksimava.hw8;

public class Employee {
    private int id;
    private String firstName;
    private String lastName;

    public Employee(int id, String firstName, String lastname) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastname;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public int getId() {
        return id;
    }
}
