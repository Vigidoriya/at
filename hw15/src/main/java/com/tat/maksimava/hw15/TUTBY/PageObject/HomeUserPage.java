package com.tat.maksimava.hw15.TUTBY.PageObject;

import org.openqa.selenium.By;

public class HomeUserPage extends Page {

    private final By accountMenuButton = By.xpath("//*[@*='uname']");
    private final By mailLink = By.xpath("//*[contains(@*,'mail.tut.by')]");
    private final By exitAccountButton = By.xpath("//*[contains(@*,'tut.by/logout')]");

    public String getAccountName() {
        return driver.findElement(accountMenuButton).getText();
    }

    public HomeUserPage accountMenu() {
        driver.findElement(accountMenuButton).click();
        return new HomeUserPage();
    }

    public MailPage openMailPage() {
        driver.findElement(mailLink).click();
        return new MailPage();
    }

    public HomePage exitAccount() {
        driver.findElement(exitAccountButton).click();
        return new HomePage();
    }
}
