package com.tat.maksimava.hw12.PageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


class HomeUserPage extends Page {
    @FindBy(xpath = "//*[@*='uname']")
    private WebElement accountMenuButton;
    @FindBy(xpath = "//*[contains(@*,'mail.tut.by')]")
    private WebElement mailLink;
    @FindBy(xpath = "//*[contains(@*,'tut.by/logout')]")
    private WebElement exitAccountButton;

    HomeUserPage(WebDriver driver) {
        super(driver);
    }

    String getAccountName() {
        return accountMenuButton.getText();
    }

    HomeUserPage accountMenu() {
        accountMenuButton.click();
        return new HomeUserPage(getDriver());
    }

    MailPage openMailPage() {
        mailLink.click();
        return new MailPage(getDriver());
    }

    HomePage exitAccount() {
        exitAccountButton.click();
        return new HomePage(getDriver());
    }
}
