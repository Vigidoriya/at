package com.tat.maksimava.hw15.TUTBY.SelenideServices;

import com.tat.maksimava.hw15.TUTBY.SelenidePageObject.HomeUserPage;

public class HomeUserPageService {
    public String logOutAndGetAccountName() {
        return new HomeUserPage().accountMenu().exitAccount().getAccountName();
    }
}
