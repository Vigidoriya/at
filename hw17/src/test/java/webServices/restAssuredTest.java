package webServices;

import io.restassured.response.Response;
import org.json.JSONObject;
import org.springframework.util.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;

import static org.hamcrest.CoreMatchers.*;


public class restAssuredTest {
    private final String url = "http://jsonplaceholder.typicode.com/posts";

    @Test
    public void verifyStatusCode() {
        given().when().get(url).then().statusCode(200);
        get(url).then().statusLine(containsString("200 OK"));
    }

    @Test
    public void verifyResponseHeader() {
        given().when().get(url).then().extract().header("Content-Type").equals("application/json; charset=utf-8");
    }

    @Test
    public void verifyResponseBody() {
        given().when().get(url).then().assertThat().body("size", equalTo(100));
    }

    @Test
    public void testCreateNewPost() {
        JSONObject jsonRequest = new JSONObject();
        jsonRequest.put("title", "foo");
        jsonRequest.put("body", "bar");
        jsonRequest.put("userId", "1");

        Response response = given().baseUri(url).body(jsonRequest.toString()).when().post();
        response.then().statusCode(201);
        Assert.notNull(response.body().asString(), "Response body is null");
        System.out.println("id= " + response.jsonPath().get("id").toString());
    }

    @Test
    public void testUpdatePost() {
        String uri = "http://jsonplaceholder.typicode.com/posts/1";
        JSONObject jsonRequest = new JSONObject();
        jsonRequest.put("id", "1");
        jsonRequest.put("title", "foo");
        jsonRequest.put("body", "bar");
        jsonRequest.put("userId", "1");

        given().baseUri(uri).body(jsonRequest.toString()).when().put()
                .then().statusCode(200).and().body("id", equalTo(1));
    }

    @Test
    public void testDeletePost() {
        String uri = "http://jsonplaceholder.typicode.com/posts/1";
        given().when().delete(uri).then().statusCode(200);
    }
}

