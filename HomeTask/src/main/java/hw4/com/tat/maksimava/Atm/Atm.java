package hw4.com.tat.maksimava.Atm;

import hw3.com.tat.maksimava.Card.Bank;
import hw3.com.tat.maksimava.Card.Card;


public class Atm extends Bank {
    Card card;

    public Atm(Card card) {
        this.card = card;
    }

    public void lessBalance(double sum) {
        if (this.card instanceof CreditCard) super.lessBalance((CreditCard) card, sum);
        else super.lessBalance(this.card, sum);
    }

    public double getBalance() {
        return super.getCardBalance(this.card);
    }

    public void addBalance(double sum) {
        super.addBalance(this.card, sum);
    }
}

