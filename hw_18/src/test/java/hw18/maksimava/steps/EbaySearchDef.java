package hw18.maksimava.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


import cucumber.runtime.CucumberException;
import hw18.maksimava.Utils.Screenshot;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import java.util.Iterator;

import static hw18.maksimava.Services.ResultsPageService.getTextResult;
import static hw18.maksimava.Services.HomePageServices.openEbayHomePage;
import static hw18.maksimava.Services.HomePageServices.searchProduct;

public class EbaySearchDef {
    private static Logger log = (Logger) LogManager.getLogger(EbaySearchDef.class);

    @Given("^I opened Ebay Home Page$")
    public void openEbay() {
        openEbayHomePage();
        log.info("Opening the ebay.com ");
        new Screenshot().getScreenshot();

    }

    @When("^I searche?d? the product \"([^\"]*)\" on the Ebay Home Page$")
    public void searchTheProduct(String query) {
        searchProduct(query);
        new Screenshot().getScreenshot();
    }


    @Then("^the term query \"([^\"]*)\" should be contained in the links in the results grid on the first EbayResultsPage$")
    public void linksInTheResultsGrid(String expectText) {
        String[] findWord = expectText.split("\\W");
        Iterator<String> iterator = getTextResult().iterator();
        while (iterator.hasNext()) {
            String result = iterator.next();
            for (int i = 0; i < findWord.length; i++) {
                try {
                    Assert.assertTrue(result.contains(findWord[i]));

                } catch (AssertionError | CucumberException exception) {
                    log.error("String: " + result + " - doesn't contain word: " + findWord[i]);
                    throw new AssertionError("String: " + result + " - doesn't contain word: " + findWord[i]);
                }
            }

        }
        log.info("The search query contains in the links");
        new Screenshot().getScreenshot();
    }
}
