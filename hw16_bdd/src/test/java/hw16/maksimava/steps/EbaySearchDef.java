package hw16.maksimava.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

import java.util.Iterator;

import static hw16.maksimava.Services.HomePageService.openEbayHomePage;
import static hw16.maksimava.Services.HomePageService.searchProduct;
import static hw16.maksimava.Services.ResultsPageService.getTextResult;

public class EbaySearchDef {
    @Given("^I opened Ebay Home Page$")
    public void openEbay() {
        openEbayHomePage();
    }

    @When("^I searche?d? the product \"([^\"]*)\" on the Ebay Home Page$")
    public void searchTheProduct(String query) {
        searchProduct(query);
    }

    @Then("^the term query \"([^\"]*)\" should be contained in the links in the results grid on the first EbayResultsPage$")
    public void linksInTheResultsGrid(String expectText) {
        String[] findWord = expectText.split("\\W");

        Iterator<String> iterator = getTextResult().iterator();
        while (iterator.hasNext()) {
            String result = iterator.next();
            for (int i = 0; i < findWord.length; i++) {
                Assert.assertTrue(result.contains(findWord[i]), "String " + result + " doesn't contain " + findWord[i]);
            }
        }
    }
}
