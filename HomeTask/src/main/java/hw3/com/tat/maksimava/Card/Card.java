package hw3.com.tat.maksimava.Card;

public class Card extends Bank {

    private String name;
    private double balance;

    public Card(String name) {
        this.setName(name);
        this.setBalance((double) 0);
        Bank.createNewCard(name, getBalance());
    }

    public Card(String name, double balance) {
        this.setName(name);
        this.setBalance(balance);
        Bank.createNewCard(name, balance);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }
}
