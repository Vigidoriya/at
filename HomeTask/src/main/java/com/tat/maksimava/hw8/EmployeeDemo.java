package com.tat.maksimava.hw8;

import java.util.*;

public class EmployeeDemo {

    public static void main(String[] args) {
        Employees employees = new Employees();
        FileEmployees fileEmployees = new FileEmployees();
        employees.employeesTable = fileEmployees.readFile();
        Scanner in = new Scanner(System.in);
        class Menu {
            List menu;

            public Menu() {
                setMenu();
            }

            private List setMenu() {
                menu = new ArrayList();
                menu.add("\n1. Add employee");
                menu.add("2. Remove employee by ID");
                menu.add("3. Show employees table");
                menu.add("4. Save file");
                menu.add("5. Exit");
                return menu;
            }

            public void showMenu() {
                for (Object s : menu) {
                    System.out.println(s);
                }
            }

            public void addEmployeeMenu() {
                Scanner in = new Scanner(System.in);
                String firstName = "", lastName = "";
                System.out.println("Enter first name or 'q' to back in menu ");
                while (firstName.isEmpty()) firstName = in.nextLine();
                if (firstName.equals("q")) return;
                System.out.println("Enter last name or 'q' to back in menu");
                while (lastName.isEmpty()) lastName = in.nextLine();
                if (lastName.equals("q")) return;
                employees.addEmployee(firstName.toLowerCase(), lastName.toLowerCase());
            }

            public void removeEmployeeMenu() {
                Scanner in = new Scanner(System.in);
                String removeEmplId = "";
                System.out.println("Enter ID employee to remove or 'q' to back in menu");
                while (removeEmplId.isEmpty()) removeEmplId = in.nextLine();
                if (removeEmplId.equals("q")) return;
                try {
                    employees.removeEmployeeById(Integer.valueOf(removeEmplId));
                } catch (NumberFormatException exeption) {
                    System.out.println("ID can be only integer");
                }
            }

            public void getMenu(String i) {
                switch (i) {
                    case "1": {
                        addEmployeeMenu();
                        break;
                    }
                    case "2": {
                        removeEmployeeMenu();
                        break;
                    }
                    case "3": {
                        employees.showEmployees();
                        break;
                    }
                    case "4": {
                        fileEmployees.writeFile(employees.employeesTable);
                        break;
                    }
                    case "5": {
                        System.exit(0);

                    }
                    default: {
                        System.out.println("Choose '1-5'");
                        break;
                    }
                }
            }
        }

        Menu menu = new Menu();
        for (; ; ) {
            menu.showMenu();
            String choice = in.nextLine();
            menu.getMenu(choice);
        }
    }
}