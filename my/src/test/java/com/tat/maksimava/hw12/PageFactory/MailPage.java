package com.tat.maksimava.hw12.PageFactory;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.Iterator;
import java.util.List;

public class MailPage extends Page {
    @FindBy(xpath = "//*[@*='#compose'][contains(@*,'ComposeButton')]")
    private WebElement writeEmailButton;
    @FindBy(xpath = "//textarea[@*='to']")
    private WebElement addressField;
    @FindBy(xpath = "//*[@*='mail-Compose-Field-Input']/*[@*='text']")
    private WebElement subjectField;
    @FindBy(xpath = "//*[@*='textbox']")
    private WebElement textField;
    @FindBy(xpath = "//*[@*='view=compose-cancel-button']")
    private WebElement closeDraftButton;
    @FindBy(xpath = "//*[(@*='save')]")
    private WebElement saveDraftButton;
    @FindBy(xpath = "//*[@*='#draft']")
    private WebElement menuDraftsButton;
    @FindBy(xpath = "//*[@*='#sent']")
    private WebElement menuSentButton;
    @FindBy(xpath = "//*[contains(@*,'Item_subject')]/*[@title]")
    private List<WebElement> contentMessage;
    @FindBy(xpath = "//*[@*='to']")
    private WebElement draftAddressField;
    @FindBy(xpath = "//*[@*='mail-Compose-Field-Input']/*[@*='text']")
    private WebElement draftSubjectField;
    @FindBy(xpath = "//*[contains(@*,'contents')]/*[not(@*)]")
    private WebElement draftTextField;
    @FindBy(xpath = "//*[contains(@*,'send-link')]/*[contains(@*,'send-button')]")
    private WebElement sendMailButton;
    @FindBy(xpath = "//*[contains(@*,'mail-User-Name ')]")
    private WebElement sentAddressField;
    @FindBy(xpath = "//*[contains(@*,'Subject-Wrapper')]/*[contains(@*,'Toolbar-Subject')]")
    private WebElement sentSubjectField;
    @FindBy(xpath = "//*[contains(@*,'mail-Message-Body-Content')]/*[not(@*)]")
    private WebElement sentTextField;
    @FindBy(xpath = "//*[@*='view=logo']")
    private WebElement homePageLink;

    MailPage(WebDriver driver) {
        super(driver);
    }

    HomeUserPage openHomePage() {
        homePageLink.click();
        return new HomeUserPage(getDriver());
    }

    MailPage createNewMail() {
        writeEmailButton.click();
        return new MailPage(getDriver());
    }

    MailPage fillMailForm(String address, String subject, String text) {
        addressField.sendKeys(address);
        subjectField.sendKeys(subject);
        textField.sendKeys(text);
        return this;
    }

    MailPage saveMailAsADraft() {
        closeDraftButton.click();
        saveDraftButton.click();
        return new MailPage(getDriver());
    }

    String getPageTitle() {
        return getDriver().getTitle();
    }

    private boolean isMailExists(String subject) {
        Iterator<WebElement> iterator = contentMessage.iterator();
        boolean find = false;
        while (iterator.hasNext()) {
            WebElement mailContent = iterator.next();
            String str = mailContent.getAttribute("title");
            if (str.equals(subject)) {
                find = true;
                mailContent.click();
                break;
            }
        }
        return (find);
    }

    String getContentDraftsMessage(String subject) {
        if (isMailExists(subject)) {
            return draftAddressField.getText() + " " + draftSubjectField.getAttribute("value") + " " + draftTextField.getText();
        } else return null;
    }

    String getContentSentMessage(String subject) {
        if (isMailExists(subject)) {
            waitForTitleContains("Письмо");
            return sentAddressField.getText() + " " + sentSubjectField.getText() + " " + sentTextField.getText();
        } else return null;
    }

    MailPage openDrafts() {
        menuDraftsButton.click();
        getDriver().navigate().refresh();
        waitForTitleContains("Черновики");
        return new MailPage(getDriver());
    }

    MailPage openSent() {
        menuSentButton.click();
        getDriver().navigate().refresh();
        return new MailPage(getDriver());
    }

    MailPage sendMail() {
        sendMailButton.click();
        return new MailPage(getDriver());
    }
}
