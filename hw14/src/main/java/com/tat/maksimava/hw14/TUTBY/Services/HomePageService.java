package com.tat.maksimava.hw14.TUTBY.Services;

import com.tat.maksimava.hw14.TUTBY.BO.Account;
import com.tat.maksimava.hw14.TUTBY.PageObject.HomePage;

public class HomePageService {


    public String logInMailAndGetAccountName(Account account) {
        if (checkLogout()) {
            return new HomePage().openAuthorizationForm().fillRegistrationForm(account).enterAccount().getAccountName();
        } else return new HomePage().getAccountName();
    }

    private boolean checkLogout() {
        return new HomePage().getAccountName().equals("Войти");
    }
}
