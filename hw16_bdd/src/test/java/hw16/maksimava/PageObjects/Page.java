package hw16.maksimava.PageObjects;

import hw16.maksimava.Browser.Browser;
import hw16.maksimava.Utils.Waits;
import org.openqa.selenium.WebDriver;

public abstract class Page {
    protected WebDriver driver = Browser.getWebDriverInstance();
    Waits wait = new Waits();
}
