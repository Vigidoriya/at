package hw16.maksimava.Services;

import hw16.maksimava.PageObjects.EbayHomePage;

public class HomePageService {
    public static void searchProduct(String query) {
        EbayHomePage homePage = new EbayHomePage();
        homePage.enterQuery(query);
        homePage.clickSearch();
    }

    public static void openEbayHomePage() {
        new EbayHomePage().open();
    }
}
