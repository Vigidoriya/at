package hw18.maksimava.Services;

import hw18.maksimava.PageObjects.EbayResultsPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ResultsPageService {
    private static Logger log = (Logger) LogManager.getLogger(ProductPageServices.class);

    public static List<String> getTextResult() {
        List<String> textResultLinks = new ArrayList<>();
        EbayResultsPage ebayResultsPage = new EbayResultsPage();
        List<WebElement> results = ebayResultsPage.resultContent();
        Iterator<WebElement> iterator = results.iterator();
        while (iterator.hasNext()) {
            WebElement result = iterator.next();
            textResultLinks.add(result.getText().toLowerCase());
        }
        log.debug(textResultLinks.size() + " result links on the first result page");
        return textResultLinks;
    }

    public static void openFirstProductResult() {
        new EbayResultsPage().openTheFirstProduct();
    }
}
