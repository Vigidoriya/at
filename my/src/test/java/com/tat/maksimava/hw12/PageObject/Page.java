package com.tat.maksimava.hw12.PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class Page {
    int timeToWaitElement=8;
    int timeToWaitTitle=5;
    private final WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    WebElement waitForElementVisibility(By locator) {
        return new WebDriverWait(getDriver(), timeToWaitElement).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    void waitForTitleContains(String title) {
        new WebDriverWait(driver, timeToWaitTitle).until(ExpectedConditions.titleContains(title));
    }
}
