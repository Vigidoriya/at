package com.tat.maksimava.hw15.TUTBY.BO;

public class MailFactory {
    private static final String SUBJECT = System.currentTimeMillis() + " home work 15";
    private static final String TEXT = "Design Patterns in Test Automation";
    private static final String ADDRESS = "maksimavavik@gmail.com";

    public static Mail getMail() {
        return new Mail(ADDRESS, SUBJECT, TEXT);
    }

}
