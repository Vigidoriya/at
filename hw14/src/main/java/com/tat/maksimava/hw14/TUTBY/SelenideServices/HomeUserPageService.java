package com.tat.maksimava.hw14.TUTBY.SelenideServices;

import com.tat.maksimava.hw14.TUTBY.SelenidePageObject.HomeUserPage;

public class HomeUserPageService {
    public String logOutAndGetAccountName() {
        return new HomeUserPage().accountMenu().exitAccount().getAccountName();
    }
}
