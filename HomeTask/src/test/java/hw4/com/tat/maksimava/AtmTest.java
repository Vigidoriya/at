package hw4.com.tat.maksimava;


import hw4.com.tat.maksimava.Atm.Atm;
import hw4.com.tat.maksimava.Atm.CreditCard;
import hw4.com.tat.maksimava.Atm.DebitCard;
import org.junit.Assert;
import org.junit.Test;

public class AtmTest {

    @Test(expected = Exception.class)
    public void testCreateDebitCardWithIncorrectBalance() {
        DebitCard debitCard1 = new DebitCard("debitCard1", -200);
    }

    @Test
    public void testCreateDebitCard() {
        DebitCard card1 = new DebitCard("debitcard1", 720);
        Assert.assertEquals("debitcard1", card1.getName());
        Assert.assertEquals(720, card1.getBalance(), 0);

        DebitCard card2 = new DebitCard("debitcard2");
        Assert.assertEquals("debitcard2", card2.getName());
        Assert.assertEquals(0, card2.getBalance(), 0);
    }

    @Test
    public void testDebitCardGetBalance() {

        DebitCard debitCard1 = new DebitCard("debitCard1");
        DebitCard debitCard2 = new DebitCard("debitCard2", 300);
        Atm atmdc1 = new Atm(debitCard1);
        Atm atmdc2 = new Atm(debitCard2);

        Assert.assertEquals(0, atmdc1.getBalance(), 0);
        Assert.assertEquals(300, atmdc2.getBalance(), 0);
    }

    @Test
    public void testDebitCardAddBalance() {
        DebitCard debitCard3 = new DebitCard("debitCard3", 200);
        DebitCard debitCard4 = new DebitCard("debitCard4", 500);
        Atm atmdc3 = new Atm(debitCard3);
        Atm atmdc4 = new Atm(debitCard4);
        atmdc3.addBalance(400);
        atmdc4.addBalance(200);

        Assert.assertEquals(600, atmdc3.getBalance(), 0);
        Assert.assertEquals(700, atmdc4.getBalance(), 0);

    }

    @Test
    public void testDebitCardLessBalance() {
        DebitCard debitCard5 = new DebitCard("debitCard5", 200);
        DebitCard debitCard6 = new DebitCard("debitCard6", 500);
        Atm atmdc5 = new Atm(debitCard5);
        Atm atmdc6 = new Atm(debitCard6);

        atmdc5.lessBalance(400);
        atmdc6.lessBalance(200);

        Assert.assertEquals(200, atmdc5.getBalance(), 0);
        Assert.assertEquals(300, atmdc6.getBalance(), 0);
    }

    @Test
    public void testCreateCreditCard() {
        CreditCard card1 = new CreditCard("creditcard1");

        Assert.assertEquals("creditcard1", card1.getName());
        Assert.assertEquals(0, card1.getBalance(), 0);

        CreditCard card2 = new CreditCard("creditcard2", 300);

        Assert.assertEquals("creditcard2", card2.getName());
        Assert.assertEquals(300, card2.getBalance(), 0);
    }

    @Test(expected = Exception.class)
    public void testCreateCreditCardWithIncorrectBalance() {
        CreditCard creditCard = new CreditCard("creditCard1", -20);
    }

    @Test
    public void testCreditCardGetBalance() {

        CreditCard creditCard1 = new CreditCard("creditCard1", 700);
        CreditCard creditCard2 = new CreditCard("creditCard2");
        Atm atmcc1 = new Atm(creditCard1);
        Atm atmcc2 = new Atm(creditCard2);

        Assert.assertEquals(700, atmcc1.getBalance(), 0);
        Assert.assertEquals(0, atmcc2.getBalance(), 0);
    }

    @Test
    public void testCreditCardAddBalance() {
        CreditCard creditCard3 = new CreditCard("creditCard3", 100);
        CreditCard creditCard4 = new CreditCard("creditCard4");
        Atm atmcc3 = new Atm(creditCard3);
        Atm atmcc4 = new Atm(creditCard4);
        atmcc3.addBalance(250);
        atmcc4.addBalance(650);

        Assert.assertEquals(350, atmcc3.getBalance(), 0);
        Assert.assertEquals(650, atmcc4.getBalance(), 0);
    }

    @Test
    public void testCreditCardLessBalance() {
        CreditCard creditCard5 = new CreditCard("creditCard5", 700);
        CreditCard creditCard6 = new CreditCard("creditCard6", 500);
        Atm atmdc5 = new Atm(creditCard5);
        Atm atmdc6 = new Atm(creditCard6);

        atmdc5.lessBalance(400);
        atmdc6.lessBalance(700);

        Assert.assertEquals(300, atmdc5.getBalance(), 0);
        Assert.assertEquals(-200, atmdc6.getBalance(), 0);
    }

    @Test(expected = Exception.class)
    public void testIncorrectSumAddBalance() {
        DebitCard debitCard7 = new DebitCard("debitCard7", 200);
        Atm atmdc7 = new Atm(debitCard7);
        atmdc7.addBalance(-200);
    }

    @Test(expected = Exception.class)
    public void testIncorrectSumDebitCardLessBalance() {
        DebitCard debitCard7 = new DebitCard("debitCard7", 200);
        Atm atmdc7 = new Atm(debitCard7);
        atmdc7.lessBalance(-200);
    }

    @Test(expected = Exception.class)
    public void testIncorrectSumCreditCardLessBalance() {
        CreditCard creditCard7 = new CreditCard("creditCard7", 207);
        Atm atmcc7 = new Atm(creditCard7);
        atmcc7.lessBalance(-100);
    }
}
