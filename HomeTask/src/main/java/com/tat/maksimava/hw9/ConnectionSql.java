package com.tat.maksimava.hw9;

import java.sql.*;

public class ConnectionSql {
    private static Connection connection;
    private static Statement statement;

    public static Statement getStatement() {
        try {
            statement = getConnection().createStatement();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return statement;
    }

    public static Connection getConnection() {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:src/main/resources/db/db_Employee.db");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return connection;
    }

    public static void closeConnection() {
        try {
            getConnection().close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
