package com.tat.maksimava.hw8;

import java.io.*;
import java.util.List;

public class FileEmployees {
    File file = new File("Employee.txt");

    public void writeFile(List<Employee> employeesTable) {
        try {
            FileWriter fileWriter = new FileWriter(file);
            for (int i = 0; i < employeesTable.size(); i++) {
                fileWriter.write(employeesTable.get(i).getId() + " " +
                        employeesTable.get(i).getFirstName() + " " +
                        employeesTable.get(i).getLastName() + "\n");
            }
            fileWriter.flush();
            fileWriter.close();
            System.out.println("File saved");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private File checkFile() {
        try {
            if (!file.exists()) file.createNewFile();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return file;
    }

    public List<Employee> readFile() {
        file = checkFile();
        Employees employees = new Employees();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file.getName())))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] tokens = line.split("\\W");
                employees.setEmployees(Integer.parseInt(tokens[0]), tokens[1], tokens[2]);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return employees.employeesTable;
    }
}