package com.tat.maksimava.hw15.TUTBY.Tests;

import com.codeborne.selenide.Configuration;
import com.tat.maksimava.hw15.TUTBY.BO.Account;
import com.tat.maksimava.hw15.TUTBY.BO.AccountFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import static com.codeborne.selenide.Selenide.close;

public class ConfigurationSelenideTest {
    Account account;

    @BeforeClass
    public void setUp() {
        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        account = AccountFactory.getAccount();
    }

    @AfterClass
    public void dummy() {
        close();
    }
}
