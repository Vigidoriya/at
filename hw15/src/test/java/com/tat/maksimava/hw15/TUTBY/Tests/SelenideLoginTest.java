package com.tat.maksimava.hw15.TUTBY.Tests;

import com.tat.maksimava.hw15.TUTBY.SelenideServices.HomePageService;
import com.tat.maksimava.hw15.TUTBY.SelenideServices.HomeUserPageService;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;

public class SelenideLoginTest extends ConfigurationSelenideTest {


    @Test
    public void testLogIn() {
        open("https://www.tut.by/", HomePageService.class);
        Assert.assertEquals(new HomePageService().getAccountName(), "Войти", "LogIn with account " + new HomePageService().getAccountName());

        String accountName = new HomePageService().logInMailAndGetAccountName(account);
        Assert.assertEquals(accountName, account.getUsername(), "Account name is " + accountName);
    }

    @Test(dependsOnMethods = "testLogIn")
    public void testLogout() {
        String loginName = new HomeUserPageService().logOutAndGetAccountName();
        Assert.assertEquals(loginName, "Войти", "Unsuccessful logOut");
    }
}
