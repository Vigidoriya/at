package com.tat.maksimava.hw15.TUTBY.Services;

import com.tat.maksimava.hw15.TUTBY.PageObject.HomeUserPage;

public class HomeUserPageService {

    public String enterMailAndGetPageTitle() {
        return new HomeUserPage().openMailPage().getTitle();
    }
}
