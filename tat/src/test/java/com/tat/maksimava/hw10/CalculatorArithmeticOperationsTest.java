package com.tat.maksimava.hw10;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.*;

public class CalculatorArithmeticOperationsTest {
    private Calculator calculator;

    @BeforeTest
    public void setUp() {
        calculator = new Calculator();
    }

    @Test(dataProvider = "doubleSumParams", dataProviderClass = TestParams.class)
    public void testSumOfTwoDoubleNumbers(double a, double b, double expected) {
        Assert.assertEquals(calculator.sum(a, b), expected, 0, "Test double sum: " + a + "+" + b);
    }

    @Test(dataProvider = "longSumParams", dataProviderClass = TestParams.class)
    public void testSumOfTwoLongNumbers(long a, long b, long expected) {
        Assert.assertEquals(calculator.sum(a, b), expected, "Test long sum: " + a + "+" + b);
    }

    @Test(dataProvider = "longSubParams", dataProviderClass = TestParams.class)
    public void testSubtractionOfTwoLongNumbers(long a, long b, long expected) {
        Assert.assertEquals(calculator.sub(a, b), expected, "Test long sub: " + a + "-" + b);
    }

    @Test(dataProvider = "doubleSubParams", dataProviderClass = TestParams.class)
    public void testSubtractionOfTwoDoubleNumbers(double a, double b, double expected) {
        Assert.assertEquals(calculator.sub(a, b), expected, 0, "Test double sub: " + a + "-" + b);
    }

    @Test(dataProvider = "longMultParams", dataProviderClass = TestParams.class)
    public void testMultiplicationOfTwoLongNumbers(long a, long b, long expected) {
        Assert.assertEquals(calculator.mult(a, b), expected, "Test long mult: " + a + "*" + b);
    }

    @Test(dataProvider = "doubleMultParams", dataProviderClass = TestParams.class)
    public void testMultiplicationOfTwoDoubleNumbers(double a, double b, double expected) {

        Assert.assertEquals(calculator.mult(a, b), expected, 0, "Test double mult: " + a + "*" + b);
    }

    @Test(dataProvider = "divLongParams", dataProviderClass = TestParams.class)
    public void testDivisionOfTwoLongNumbers(long a, long b, long expected) {
        Assert.assertEquals(calculator.div(a, b), expected, "Test long div: " + a + "/" + b);
    }

    @Test(dataProvider = "divDoubleParams", dataProviderClass = TestParams.class)
    public void testDivisionOfTwoDoubleNumbers(double a, double b, double expected) {

        Assert.assertEquals(calculator.div(a, b), expected, 0, "Test double div: " + a + "/" + b);
    }

    @Test(expectedExceptions = NumberFormatException.class, expectedExceptionsMessageRegExp = "Attempt to divide by zero")
    public void testDivisionLongNumberByZero() {
        calculator.div(12, 0);
    }

    @Test(expectedExceptions = NumberFormatException.class, expectedExceptionsMessageRegExp = "Attempt to divide by zero")
    public void testDivisionDoubleNumberByZero() {
        calculator.div(4.1, 0.0);
    }

    @Test(dataProvider = "powParams", dataProviderClass = TestParams.class)
    public void testPow(double a, double b, double expected) {
        Assert.assertEquals(calculator.pow(a, b), expected, 0, "Test pow(" + a + "," + b + ")");
    }

    @Test(dataProvider = "sqrtParams", dataProviderClass = TestParams.class)
    public void testSqrt(double a, double expected) {
        Assert.assertEquals(calculator.sqrt(a), expected, 0, "Test sqrt(" + a + ")");

    }

    @Test(dataProvider = "positiveTestParams", dataProviderClass = TestParams.class)
    public void testIsPositiveNumber(long a, boolean expected) {
        Assert.assertEquals(calculator.isPositive(a), expected, "Test isn't positive " + a);
    }

    @Test(dataProvider = "negativeTestParams", dataProviderClass = TestParams.class)
    public void testIsNegativeNumber(long a, boolean expected) {
        Assert.assertEquals(calculator.isNegative(a), expected, "Test isn't negative " + a);
    }
}
