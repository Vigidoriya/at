package hw16.maksimava.Services;

import hw16.maksimava.PageObjects.EbayCartPage;

public class CartPageServices {
    public static String getTextCart() {
        return new EbayCartPage().getCartContent().toLowerCase();

    }

    public static String getPageTitle() {
        return new EbayCartPage().getPageTitle();
    }

    public static String getMessage() {
        return new EbayCartPage().getMessage();
    }

}
