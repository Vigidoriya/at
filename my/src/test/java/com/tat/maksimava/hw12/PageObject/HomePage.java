package com.tat.maksimava.hw12.PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


class HomePage extends Page {
    private final By enterAccountButton = By.xpath("//*[@*='enter']");
    private final By loginField = By.xpath("//*[(@*='login')][@*='text']");
    private final By passwordField = By.xpath("//*[(@*='password')]");
    private final By submitButton = By.xpath("//*[@*='b-hold']/*[@*='submit']");

    HomePage(WebDriver driver) {
        super(driver);
    }

    boolean checkLogout() {
        waitForElementVisibility(enterAccountButton);
        return (getDriver().findElement(enterAccountButton).getText().equals("Войти"));
    }

    HomePage openAuthorizationForm() {
        getDriver().findElement(enterAccountButton).click();
        return new HomePage(getDriver());
    }

    HomeUserPage enterAccount() {
        getDriver().findElement(submitButton).click();
        return new HomeUserPage(getDriver());
    }

    HomePage fillRegistrationForm(String login, String password) {
        getDriver().findElement(loginField).sendKeys(login);
        getDriver().findElement(passwordField).sendKeys(password);
        return new HomePage(getDriver());
    }
}