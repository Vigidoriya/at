package hw18.maksimava.Services;

import hw18.maksimava.PageObjects.EbayCartPage;

public class CartServices {
    public static String getTextCart() {
        return new EbayCartPage().getCartContent().toLowerCase();

    }

    public static String getPageTitle() {
        return new EbayCartPage().getPageTitle();
    }

    public static String getMessage() {
        return new EbayCartPage().getMessage();
    }

}
