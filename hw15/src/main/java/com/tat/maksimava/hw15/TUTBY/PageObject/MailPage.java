package com.tat.maksimava.hw15.TUTBY.PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Iterator;
import java.util.List;

public class MailPage extends Page {
    private final By writeEmailButton = By.xpath("//*[@*='#compose'][contains(@*,'ComposeButton')]");
    private final By addressField = By.xpath("//textarea[@*='to']");
    private final By subjectField = By.xpath("//*[@*='mail-Compose-Field-Input']/*[@*='text']");
    private final By textField = By.xpath("//*[@*='textbox']");
    private final By closeDraftButton = By.xpath("//*[@*='view=compose-cancel-button']");
    private final By saveDraftButton = By.xpath("//*[(@*='save')]");
    private final By menuDraftsButton = By.xpath("//*[@*='#draft']");
    private final By menuSentButton = By.xpath("//*[@*='#sent']");
    private final By sendMailButton = By.xpath("//*[contains(@*,'send-link')]/*[contains(@*,'send-button')]");
    private final By homePageLink = By.xpath("//*[@*='view=logo']");
    private final By draftAddressField = By.xpath("//*[@*='to']");
    private final By draftSubjectField = By.xpath("//*[@*='mail-Compose-Field-Input']/*[@*='text']");
    private final By draftTextField = By.xpath("//*[contains(@*,'contents')]/*[not(@*)]");
    private final By sentAddressField = By.xpath("//*[contains(@*,'mail-User-Name ')]");
    private final By sentSubjectField = By.xpath("//*[contains(@*,'Subject-Wrapper')]/*[contains(@*,'Toolbar-Subject')]");
    private final By sentTextField = By.xpath("//*[contains(@*,'mail-Message-Body-Content')]/*[not(@*)]");
    private final By contentMessage = By.xpath("//*[contains(@*,'Item_subject')]/*[@title]");
    ;

    private String getDraftAddress() {
        return driver.findElement(draftAddressField).getText();
    }

    private String getDraftSubject() {
        return driver.findElement(draftSubjectField).getAttribute("value");
    }

    private String getDraftText() {
        return driver.findElement(draftTextField).getText();
    }

    private String getSentAddress() {
        return driver.findElement(sentAddressField).getText();
    }

    private String getSentSubject() {
        return driver.findElement(sentSubjectField).getText();
    }

    private String getSentText() {
        return driver.findElement(sentTextField).getText();
    }

    public boolean isMailExists(String subject) {
        List<WebElement> contentListMessages = driver.findElements(contentMessage);
        Iterator<WebElement> iterator = contentListMessages.iterator();
        boolean find = false;
        while (iterator.hasNext()) {
            WebElement mailContent = iterator.next();
            String str = mailContent.getAttribute("title");
            if (str.equals(subject)) {
                find = true;
                mailContent.click();
                break;
            }
        }
        return (find);
    }

    public String getDraftContentMessage() {
        return getDraftAddress() + " " + getDraftSubject() + " " + getDraftText();
    }

    public String getSentContentMessage() {
        waits.waitForTitleContains("Письмо");
        return getSentAddress() + " " + getSentSubject() + " " + getSentText();
    }

    public HomeUserPage openHomeUserPage() {
        driver.findElement(homePageLink).click();
        return new HomeUserPage();
    }

    public MailPage createNewMail() {
        driver.findElement(writeEmailButton).click();
        return new MailPage();
    }

    public MailPage fillMailForm(String address, String subject, String text) {
        driver.findElement(addressField).sendKeys(address);
        driver.findElement(subjectField).sendKeys(subject);
        driver.findElement(textField).sendKeys(text);
        return this;
    }


    public MailPage saveMailAsADraft() {
        driver.findElement(closeDraftButton).click();
        driver.findElement(saveDraftButton).click();
        return new MailPage();
    }

    public MailPage openDrafts() {
        driver.findElement(menuDraftsButton).click();
        driver.navigate().refresh();
        waits.waitForTitleContains("Черновики");
        return new MailPage();
    }

    public MailPage openSent() {
        driver.findElement(menuSentButton).click();
        driver.navigate().refresh();
        waits.waitForTitleContains("Отправленные");
        return new MailPage();
    }

    public MailPage sendMail() {
        driver.findElement(sendMailButton).click();
        return new MailPage();
    }
}
