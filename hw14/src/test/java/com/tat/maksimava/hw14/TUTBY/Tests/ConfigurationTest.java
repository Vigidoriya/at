package com.tat.maksimava.hw14.TUTBY.Tests;

import com.tat.maksimava.hw14.TUTBY.BO.Account;
import com.tat.maksimava.hw14.TUTBY.BO.AccountFactory;
import com.tat.maksimava.hw14.TUTBY.BO.Mail;
import com.tat.maksimava.hw14.TUTBY.BO.MailFactory;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import static com.tat.maksimava.hw14.TUTBY.Browser.Browser.close;
import static com.tat.maksimava.hw14.TUTBY.Browser.Browser.getWebDriverInstance;

public class ConfigurationTest {
    private static final String MAIL_URL = "https://www.tut.by/";
    Account account;
    Mail mail;

    @BeforeClass
    public void setUp() {
        WebDriver driver = getWebDriverInstance();
        driver.get(MAIL_URL);
        account = AccountFactory.getAccount();
        mail = MailFactory.getMail();
    }

    @AfterClass
    public void reset() {
        close();
    }
}
