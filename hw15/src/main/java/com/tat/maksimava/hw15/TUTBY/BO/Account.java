package com.tat.maksimava.hw15.TUTBY.BO;

public class Account {

    private String login;
    private String password;
    private String username;

    Account(String login, String password, String username) {
        this.login = login;
        this.password = password;
        this.username = username;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

}
