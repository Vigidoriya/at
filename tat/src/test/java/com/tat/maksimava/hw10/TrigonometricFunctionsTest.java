package com.tat.maksimava.hw10;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TrigonometricFunctionsTest {
    private double a;
    private Calculator calculator;

    @BeforeTest
    public void setUp() {
        calculator = new Calculator();
    }

    TrigonometricFunctionsTest(double a) {
        this.a = a;
    }

    @Test(priority = 1)
    public void testSin() {
        Assert.assertEquals(calculator.sin(a), Math.sin(a), 0, "Test sin(" + a + ")");
    }

    @Test(priority = 2)
    public void testCos() {
        Assert.assertEquals(calculator.cos(a), Math.cos(a), 0, "Test cos(" + a + ")");
    }

    @Test(priority = 3)
    public void testTg() {
        Assert.assertEquals(calculator.tg(a), Math.tan(a), 0, "Test tg(" + a + ")");
    }

    @Test(priority = 4)
    public void testCtg() {
        Assert.assertEquals(calculator.ctg(a), 1 / Math.tan(a), 0, "Test ctg(" + a + ")");
    }

}
