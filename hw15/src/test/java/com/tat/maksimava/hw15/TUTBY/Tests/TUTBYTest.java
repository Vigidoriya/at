package com.tat.maksimava.hw15.TUTBY.Tests;

import com.tat.maksimava.hw15.TUTBY.Services.HomePageService;
import com.tat.maksimava.hw15.TUTBY.Services.HomeUserPageService;
import com.tat.maksimava.hw15.TUTBY.Services.MailPageService;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TUTBYTest extends ConfigurationTest {

    @Test
    public void testTutBy() {
        String messageContent;
        String accountName;

        accountName = new HomePageService().logInMailAndGetAccountName(account);
        Assert.assertEquals(accountName, account.getUsername(), "User name is " + accountName);

        String pageTitle = new HomeUserPageService().enterMailAndGetPageTitle();
        Assert.assertTrue(pageTitle.contains("Почта"), "Page title isn't 'Почта'");

        MailPageService mailService = new MailPageService();
        mailService.createDraft(address, subject, text);
        messageContent = mailService.sendDraftAndGetDraftContent(subject);
        Assert.assertTrue(messageContent.contains(address), messageContent + " doesn't contains '" + address + "' in Drafts");
        Assert.assertTrue(messageContent.contains(text), messageContent + " doesn't contains '" + text + "' in Drafts");

        messageContent = mailService.findMailInDraftContent(subject);
        Assert.assertNull(messageContent, "Mail with subject '" + subject + "' contains in Drafts");

        messageContent = mailService.findMailInSentContent(subject);
        Assert.assertTrue(messageContent.contains(address), messageContent + " doesn't contains '" + address + "' in Sent");
        Assert.assertTrue(messageContent.contains(text), messageContent + " doesn't contains '" + text + "' in Sent");

        accountName = new MailPageService().logOutMailAndGetAccountName();
        Assert.assertNotEquals(accountName, account.getUsername(), "Unsuccessful logOutAndGetAccountName");
    }
}
