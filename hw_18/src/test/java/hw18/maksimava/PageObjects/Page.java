package hw18.maksimava.PageObjects;

import hw18.maksimava.Browser.Browser;
import hw18.maksimava.Utils.Waits;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public abstract class Page {
    protected WebDriver driver = Browser.getWebDriverInstance();
    Waits wait = new Waits();
    Logger log = (Logger) LogManager.getLogger(Page.class);
}
