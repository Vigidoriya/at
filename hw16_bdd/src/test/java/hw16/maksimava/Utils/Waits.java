package hw16.maksimava.Utils;

import hw16.maksimava.Browser.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waits {
    protected WebDriver driver = Browser.getWebDriverInstance();

    public WebElement waitForElementVisibility(By locator) {
        return new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
}
