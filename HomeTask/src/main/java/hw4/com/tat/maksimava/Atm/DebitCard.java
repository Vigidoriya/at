package hw4.com.tat.maksimava.Atm;

import hw3.com.tat.maksimava.Card.Card;


public class DebitCard extends Card {

    public DebitCard(String name) {
        super(name);
    }

    public DebitCard(String name, double balance) {
        super(name, balance);
    }

}