package com.tat.maksimava.hw14.TUTBY.SelenidePageObject;

import com.tat.maksimava.hw14.TUTBY.BO.Account;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class HomePage {

    private final By enterAccountButton = By.xpath("//*[@*='enter']");
    private final By submitButton = By.xpath("//*[@*='b-hold']/*[@*='submit']");
    private final By loginField = By.xpath("//*[(@*='login')][@*='text']");
    private final By passwordField = By.xpath("//*[(@*='password')]");

    public HomeUserPage enterAccount() {
        $(submitButton).click();
        return page(HomeUserPage.class);
    }

    public HomePage fillRegistrationForm(Account account) {
        $(loginField).setValue(account.getLogin());
        $(passwordField).setValue(account.getPassword());
        return page(HomePage.class);
    }

    public String getAccountName() {
        return $(enterAccountButton).getText();
    }

    public HomePage openAuthorizationForm() {
        $(enterAccountButton).click();
        return page(HomePage.class);
    }
}
