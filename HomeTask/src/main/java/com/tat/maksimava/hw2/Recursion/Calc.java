package com.tat.maksimava.hw2.Recursion;

public class Calc {
    long fibonacci(int n) {
        long ch = (long) ((Math.pow(1 + Math.sqrt(5), n) - Math.pow(1 - Math.sqrt(5), n)) / (Math.pow(2, n) * Math.sqrt(5)));
        return ch;
    }

    int recursion(int n, long param) {

        if ((fibonacci(n)) >= param)
            return n - 1;
        else {
            return recursion(n + 1, param);
        }
    }
}
