package com.tat.maksimava.hw12.PageObject;

import org.openqa.selenium.*;

import java.util.Iterator;
import java.util.List;

class MailPage extends Page {

    private final By writeEmailButton = By.xpath("//*[@*='#compose'][contains(@*,'ComposeButton')]");
    private final By addressField = By.xpath("//textarea[@*='to']");
    private final By subjectField = By.xpath("//*[@*='mail-Compose-Field-Input']/*[@*='text']");
    private final By textField = By.xpath("//*[@*='textbox']");
    private final By closeDraftButton = By.xpath("//*[@*='view=compose-cancel-button']");
    private final By saveDraftButton = By.xpath("//*[(@*='save')]");
    private final By menuDraftsButton = By.xpath("//*[@*='#draft']");
    private final By menuSentButton = By.xpath("//*[@*='#sent']");
    private final By contentMessage = By.xpath("//*[contains(@*,'Item_subject')]/*[@title]");
    private final By draftAddressField = By.xpath("//*[@*='to']");
    private final By draftSubjectField = By.xpath("//*[@*='mail-Compose-Field-Input']/*[@*='text']");
    private final By draftTextField = By.xpath("//*[contains(@*,'contents')]/*[not(@*)]");
    private final By sendMailButton = By.xpath("//*[contains(@*,'send-link')]/*[contains(@*,'send-button')]");
    private final By sentAddressField = By.xpath("//*[contains(@*,'mail-User-Name ')]");
    private final By sentSubjectField = By.xpath("//*[contains(@*,'Subject-Wrapper')]/*[contains(@*,'Toolbar-Subject')]");
    private final By sentTextField = By.xpath("//*[contains(@*,'mail-Message-Body-Content')]/*[not(@*)]");
    private final By homePageLink = By.xpath("//*[@*='view=logo']");

    MailPage(WebDriver driver) {
        super(driver);
    }

    HomeUserPage openHomePage() {
        waitForElementVisibility(homePageLink).click();
        return new HomeUserPage(getDriver());
    }

    MailPage createNewMail() {
        waitForElementVisibility(writeEmailButton).click();
        return new MailPage(getDriver());
    }

    MailPage fillMailForm(String address, String subject, String text) {
        getDriver().findElement(addressField).sendKeys(address);
        getDriver().findElement(subjectField).sendKeys(subject);
        getDriver().findElement(textField).sendKeys(text);
        return this;
    }

    MailPage saveMailAsADraft() {
        getDriver().findElement(closeDraftButton).click();
        getDriver().findElement(saveDraftButton).click();
        return new MailPage(getDriver());
    }

    String getPageTitle() {
        return getDriver().getTitle();
    }

    private boolean isMailExists(String subject) {
        List<WebElement> contentList = getDriver().findElements(contentMessage);
        Iterator<WebElement> iterator = contentList.iterator();
        boolean find = false;
        while (iterator.hasNext()) {
            WebElement mailContent = iterator.next();
            String str = mailContent.getAttribute("title");
            if (str.equals(subject)) {
                find = true;
                mailContent.click();
                break;
            }
        }
        return (find);
    }

    String getContentDraftsMessage(String subject) {
        if (isMailExists(subject)) {

            return getDriver().findElement(draftAddressField).getText() + " "
                    + getDriver().findElement(draftSubjectField).getAttribute("value") + " "
                    + getDriver().findElement(draftTextField).getText();
        } else return null;
    }

    String getContentSentMessage(String subject) {
        if (isMailExists(subject)) {
            waitForTitleContains("Письмо");
            return getDriver().findElement(sentAddressField).getText() + " "
                    + getDriver().findElement(sentSubjectField).getText() + " "
                    + getDriver().findElement(sentTextField).getText();
        }
        return null;
    }

    MailPage openDrafts() {
        getDriver().findElement(menuDraftsButton).click();
        getDriver().navigate().refresh();
        waitForTitleContains("Черновики");
        return new MailPage(getDriver());
    }

    MailPage openSent() {
        getDriver().findElement(menuSentButton).click();
        getDriver().navigate().refresh();
        return new MailPage(getDriver());
    }

    MailPage sendMail() {
        waitForElementVisibility(sendMailButton).click();
        return new MailPage(getDriver());
    }
}
