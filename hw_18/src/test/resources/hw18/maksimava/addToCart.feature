@ebay
Feature: Add to cart

  Scenario:Adding product to the cart
    Given I opened Ebay Home Page
    And I searched the product "sony playstation 4 pro" on the Ebay Home Page

    When I click the first link in the results grid on the Ebay Results Page
    And  I get product name in the product description label on the Ebay Product Page
    And I click the button 'Add to cart' in the product description on the Ebay Product Page

    Then The cart page should be opened
    And The term query "sony playstation 4 pro" should be contains in the cart grid on the Ebay Cart Page
    And The product name should be in the message field on the Ebay Cart Page



