package com.tat.maksimava.hw7.Sorting;

import java.util.Comparator;

public class Sorting implements Comparator {
    Integer getSumNumber(Integer number) {
        Integer sum = 0;
        while (number != 0) {
            sum += (number % 10);
            number /= 10;
        }
        return sum;
    }

    public int compare(Object o1, Object o2) {
        Integer first = (Integer) o1;
        Integer second = (Integer) o2;
        int result = getSumNumber(first).compareTo(getSumNumber(second));
        return result;
    }
}
