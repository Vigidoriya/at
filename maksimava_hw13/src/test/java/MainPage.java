import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.*;

public class MainPage extends Page {
    private final By menuDroppable = By.xpath("//a[text()='Droppable']");
    private final By menuCheckboxradio = By.xpath("//a[text()='Checkboxradio']");
    private final By menuSelectmenu = By.xpath("//a[text()='Selectmenu']");
    private final By menuTooltip = By.xpath("//a[text()='Tooltip']");
    private final By menuTabs = By.xpath("//a[text()='Tabs']");
    private final By menuSortable = By.xpath("//a[text()='Sortable']");
    private final By demoFrame = By.className("demo-frame");
    private final By draggableElement = By.id("draggable");
    private final By droppableElement = By.id("droppable");
    private final By radio = By.xpath("//fieldset/label[contains(@for,'radio')]");
    private final By radioChecked = By.xpath("//label[contains(@class,'state-active')]");
    private final By checkbox = By.xpath("//fieldset[2]/label");
    private final By checkboxChecked = By.xpath("//label[contains(@class,'state-active')][contains(@for,'checkbox')]");
    private final By speedbutton = By.id("speed-button");
    private final By speedmenu = By.xpath("//*[@id='speed-menu']/li[@class='ui-menu-item']/div[@class='ui-menu-item-wrapper']");
    private final By numberbutton = By.id("number-button");
    private final By numbermenu = By.xpath("//*[@id='number-menu']/li[@class='ui-menu-item']/div[@class='ui-menu-item-wrapper']");
    private final By speedchoice = By.xpath("//span[@id='speed-button']/span[@class='ui-selectmenu-text']");
    private final By numberchoice = By.xpath("//span[@id='number-button']/span[@class='ui-selectmenu-text']");
    private final By inputElement = By.id("age");
    private final By tooltipElement = By.cssSelector("div.ui-tooltip-content");
    private final By sortableItems = By.xpath("//*[@id='sortable']/li");
    private final By visibleTabText = By.xpath("//div[@aria-hidden='false']/p");
    private final By tabElements = By.xpath("//a[@class='ui-tabs-anchor']");
    private final By selectedTab = By.xpath("//li[@aria-selected='true']/a[@class='ui-tabs-anchor']");

    Actions actions = new Actions(getDriver());

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public List menuCheckboxradio(String choiceRadio, String choiceCheckbox) {
        driver.switchTo().defaultContent();
        getDriver().findElement(menuCheckboxradio).click();
        getDriver().switchTo().frame(getDriver().findElement(demoFrame));
        return checkboxradio(choiceRadio, choiceCheckbox);
    }

    private List checkboxradio(String choiceRadio, String choiceCheckbox) {
        List<WebElement> checkboxRadio = new ArrayList<>();
        try {
            List<WebElement> radioGroup = getDriver().findElements(radio);
            Iterator<WebElement> iterator = radioGroup.iterator();
            int match = 0;
            while (iterator.hasNext()) {
                WebElement element = iterator.next();
                if (element.getText().equals(choiceRadio)) {
                    actions.click(element).build().perform();
                    match++;
                }
            }
            if (match == 0) System.out.println(choiceRadio + " don't found");
            else match = 0;
            List<WebElement> checkboxGroup = getDriver().findElements(checkbox);
            Iterator<WebElement> iterator1 = checkboxGroup.iterator();
            while (iterator1.hasNext()) {
                WebElement element = iterator1.next();
                if (element.getText().equals(choiceCheckbox)) {
                    actions.click(element).build().perform();
                    match++;
                }
            }
            if (match == 0) System.out.println(choiceCheckbox + " don't found");
            checkboxRadio.add(getDriver().findElement(radioChecked));
            checkboxRadio.add(getDriver().findElement(checkboxChecked));


        } catch (NoSuchElementException ex) {
        }
        ;
        return checkboxRadio;
    }

    public WebElement menuDroppable() {
        driver.switchTo().defaultContent();
        getDriver().findElement(menuDroppable).click();
        getDriver().switchTo().frame(getDriver().findElement(demoFrame));
        return droppable();
    }

    private WebElement droppable() {
        WebElement draggable = getDriver().findElement(draggableElement);
        WebElement droppable = getDriver().findElement(droppableElement);
        actions.dragAndDrop(draggable, droppable).build().perform();
        return droppable;
    }

    public List<WebElement> menuSelectmenu(String speed, String number) {
        driver.switchTo().defaultContent();
        getDriver().findElement(menuSelectmenu).click();
        getDriver().switchTo().frame(getDriver().findElement(demoFrame));
        return selectmenu(speed, number);
    }

    private List<WebElement> selectmenu(String speed, String number) {
        List<WebElement> selectMenu = new ArrayList<>();
        try {
            WebElement speedButton = driver.findElement(speedbutton);
            actions.click(speedButton).build().perform();
            List<WebElement> speedMenu = getDriver().findElements(speedmenu);
            Iterator<WebElement> iterator1 = speedMenu.iterator();
            int match = 0;
            while (iterator1.hasNext()) {
                WebElement element = iterator1.next();

                if (element.getText().equals(speed)) {
                    actions.click(element).build().perform();
                    match++;
                }
            }
            if (match == 0) System.out.println(speed + " don't found");
            else match = 0;
            WebElement numberButton = driver.findElement(numberbutton);
            actions.click(numberButton).build().perform();
            List<WebElement> numberMenu = getDriver().findElements(numbermenu);
            Iterator<WebElement> iterator2 = numberMenu.iterator();
            while (iterator2.hasNext()) {
                WebElement element = iterator2.next();
                if (element.getText().equals(number)) {
                    actions.click(element).build().perform();
                    match++;
                }
            }
            if (match == 0) System.out.println(number + " don't found");
            selectMenu.add(getDriver().findElement(speedchoice));
            selectMenu.add(getDriver().findElement(numberchoice));
        } catch (NoSuchElementException ex) {
        }
        return selectMenu;
    }


    public WebElement menuTooltip() {
        driver.switchTo().defaultContent();
        getDriver().findElement(menuTooltip).click();
        getDriver().switchTo().frame(getDriver().findElement(demoFrame));
        return tooltip();
    }

    private WebElement tooltip() {
        WebElement input = driver.findElement(inputElement);
        actions.moveToElement(input).build().perform();
        WebElement tooltip = getDriver().findElement(tooltipElement);
        return tooltip;
    }


    public List<WebElement> menuTabs(String tab) {
        driver.switchTo().defaultContent();
        getDriver().findElement(menuTabs).click();
        getDriver().switchTo().frame(getDriver().findElement(demoFrame));
        return tabs(tab);
    }

    private List<WebElement> tabs(String tab) {
        List<WebElement> tabs = new ArrayList<>();
        try {
            List<WebElement> tablist = getDriver().findElements(tabElements);
            int match = 0;
            Iterator<WebElement> iterator = tablist.iterator();
            while (iterator.hasNext()) {
                WebElement element = iterator.next();
                if (element.getText().equals(tab)) {
                    actions.click(element).build().perform();
                    match++;
                }
            }
            if (match == 0) System.out.println(tab + " don't found");

            WebElement tabTextElement = getDriver().findElement(visibleTabText);

            tabs.add(getDriver().findElement(selectedTab));
            tabs.add(tabTextElement);
        } catch (NoSuchElementException ex) {
        }
        ;
        return tabs;
    }

    public List menuSortable(String item1, String item2) {
        driver.switchTo().defaultContent();
        getDriver().findElement(menuSortable).click();
        getDriver().switchTo().frame(getDriver().findElement(demoFrame));
        return sortable(item1, item2);
    }

    private List<WebElement> sortable(String item1, String item2) {

        List<WebElement> sortable = getDriver().findElements(sortableItems);
        try {
            WebElement first = null, second = null;
            Iterator<WebElement> iterator = sortable.iterator();

            while (iterator.hasNext()) {
                WebElement element = iterator.next();
                if (element.getText().equals(item1)) {
                    first = element;

                } else if (element.getText().equals(item2)) {
                    second = element;

                }
            }
            if (first == null) {
                System.out.println(item1 + " don't found");
            }
            if (second == null) System.out.println(item2 + " don't found");
            actions.clickAndHold(first).moveToElement(second).release().build().perform();

            sortable = getDriver().findElements(sortableItems);
        } catch (NoSuchElementException | NullPointerException | IllegalArgumentException ex) {
        }

        return sortable;
    }
}
