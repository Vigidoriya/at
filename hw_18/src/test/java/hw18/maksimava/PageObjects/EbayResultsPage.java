package hw18.maksimava.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class EbayResultsPage extends Page {
    private final By searchResults = By.cssSelector(".s-item__title");

    public List<WebElement> resultContent() {
        return driver.findElements(searchResults);
    }

    public EbayProductPage openTheFirstProduct() {
        resultContent().get(0).click();
        log.trace("Select the first result link");
        return new EbayProductPage();
    }
}
