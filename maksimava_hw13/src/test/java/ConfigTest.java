import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;


public class ConfigTest {

    WebDriver driver;
    private static final String TEST_URL = "http://jqueryui.com";

    @BeforeClass
    public void setUp() {
        if (driver == null) {
            System.setProperty("webdriver.chrome.driver", "src/test/resources/driver/chromedriver.exe");
            driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.manage().window().maximize();
            driver.get(TEST_URL);
        }
    }

    @AfterClass
    public void reset() {
        driver.quit();
        driver = null;
    }
}
