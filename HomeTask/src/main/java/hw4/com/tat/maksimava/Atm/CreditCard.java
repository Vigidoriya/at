package hw4.com.tat.maksimava.Atm;

import hw3.com.tat.maksimava.Card.Card;

public class CreditCard extends Card {

    public CreditCard(String name) {
        super(name);
    }

    public CreditCard(String name, double balance) {
        super(name, balance);
    }
}
