package com.tat.maksimava.hw12.PageFactory;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MailTest extends ConfigTest {
    private static final String EMAIL = "epamTraining@tut.by";
    private static final String PASSWORD = "epamTraining2018";
    private static final String USER = "Еpam Training";
    private static final String SUBJECT = "home work 12 " + System.currentTimeMillis();
    private static final String TEXT = "page factory " + System.currentTimeMillis();
    private static final String ADDRESS = "maksimavavik@gmail.com";

    @Test
    public void testMail() {
        Assert.assertTrue(new HomePage(driver).checkLogout());
        HomeUserPage homeUserPage = new HomePage(driver).openAuthorizationForm().fillRegistrationForm(EMAIL, PASSWORD).enterAccount();
        Assert.assertEquals(USER, homeUserPage.getAccountName(), "User name is " + homeUserPage.getAccountName());

        MailPage mailPage = homeUserPage.openMailPage();
        Assert.assertTrue(mailPage.getPageTitle().contains("Почта"), "Page title isn't 'Почта'");

        mailPage.createNewMail().fillMailForm(ADDRESS, SUBJECT, TEXT).saveMailAsADraft();
        String content = new MailPage(driver).openDrafts().getContentDraftsMessage(SUBJECT);

        Assert.assertTrue(content.contains(ADDRESS), content + " doesn't contains '" + ADDRESS + "' in Drafts");
        Assert.assertTrue(content.contains(TEXT), content + " doesn't contains '" + TEXT + "' in Drafts");

        mailPage.sendMail();
        content = new MailPage(driver).openDrafts().getContentDraftsMessage(SUBJECT);
        Assert.assertNull(content, "Mail with subject '" + SUBJECT + "' contains in Drafts");

        content = new MailPage(driver).openSent().getContentSentMessage(SUBJECT);
        Assert.assertTrue(content.contains(ADDRESS), content + " doesn't contains '" + ADDRESS + "' in Sent");
        Assert.assertTrue(content.contains(TEXT), content + " doesn't contains '" + TEXT + "' in Sent");

        Assert.assertTrue(mailPage.openHomePage().accountMenu().exitAccount().checkLogout(), "Unsuccessful logOut");
    }
}
