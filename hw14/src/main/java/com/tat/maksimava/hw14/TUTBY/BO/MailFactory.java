package com.tat.maksimava.hw14.TUTBY.BO;

public class MailFactory {
    private static final String SUBJECT = "home work 14 " + System.currentTimeMillis();
    private static final String TEXT = "Frameworks AT " + System.currentTimeMillis();
    private static final String ADDRESS = "maksimavavik@gmail.com";

    public static Mail getMail() {
        return new Mail(ADDRESS, SUBJECT, TEXT);
    }

}
