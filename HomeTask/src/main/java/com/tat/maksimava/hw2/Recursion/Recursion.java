package com.tat.maksimava.hw2.Recursion;

public class Recursion {
    public static void main(String[] args) {

        long maxInteger = Integer.MAX_VALUE;
        long maxLong = Long.MAX_VALUE;
        int firstNumber = 1;

        Calc c = new Calc();
        System.out.println(c.recursion(firstNumber, maxInteger) + " numbers in type int");
        System.out.println(+c.recursion(firstNumber, maxLong) + " numbers in type long");
    }
}

