package hw16.maksimava.PageObjects;

import org.openqa.selenium.By;

public class EbayHomePage extends Page {
    private static final String BASE_URL = "https://www.ebay.com";
    private final By searchField = By.xpath("//input[@type='text']");
    private final By searchButton = By.xpath("//input[@type='submit']");

    public EbayHomePage open() {
        driver.get(BASE_URL);
        return this;
    }

    public EbayHomePage enterQuery(String query) {
        wait.waitForElementVisibility(searchField).sendKeys(query);
        return new EbayHomePage();
    }

    public EbayResultsPage clickSearch() {
        wait.waitForElementVisibility(searchButton).click();
        return new EbayResultsPage();
    }

}
