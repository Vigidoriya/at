package webServices;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.testng.Assert;
import org.testng.annotations.Test;

public class restTempateTest {
    private final String url = "http://jsonplaceholder.typicode.com/posts";

    @Test
    public void testGetRequest() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getForEntity(url, String.class);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        Assert.assertTrue(response.getHeaders().get("Content-Type").contains("application/json; charset=utf-8"));
        JSONArray jsonObject = new JSONArray(response.getBody());
        Assert.assertEquals(jsonObject.length(), 100);
    }

    @Test
    public void testCreateNewPost() {

        String json = "data: {'title': 'foo', 'body': 'bar','userId': 1}";
        HttpEntity<String> entity = new HttpEntity<>(json);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForEntity(url, entity, String.class);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, null, String.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.CREATED);
        Assert.assertFalse(response.getBody().isEmpty());
        JSONObject jsonObject = new JSONObject(response.getBody());
        System.out.println(jsonObject.get("id"));
    }

    @Test
    public void testUpdatePost() {
        final String uri = "http://jsonplaceholder.typicode.com/posts/1";
        String json = "data: { 'id': 1, 'title': 'foo', 'body': 'bar','userId': 1}";
        HttpEntity<String> entity = new HttpEntity<>(json);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.put(uri, entity);
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.PUT, null, String.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        JSONObject jsonObject = new JSONObject(response.getBody());
        Assert.assertEquals(jsonObject.get("id"), 1);
    }

    @Test
    public void testDeletePost() {
        final String uri = "http://jsonplaceholder.typicode.com/posts/1";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(uri);
        ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.DELETE, null, String.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
    }
}
