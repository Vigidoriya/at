package hw3.com.tat.maksimava;

import hw3.com.tat.maksimava.Card.Bank;
import hw3.com.tat.maksimava.Card.Card;
import org.junit.Assert;
import org.junit.Test;


public class CardTest {

    @Test
    public void testCreateNew() {
        Card testCard1 = new Card("testCard1", 2000);
        Card testCard2 = new Card("testCard2");
        Assert.assertEquals("testCard1", testCard1.getName());
        Assert.assertEquals(2000, testCard1.getBalance(), 0);
        Assert.assertEquals("testCard2", testCard2.getName());
        Assert.assertEquals(0, testCard2.getBalance(), 0);
    }

    @Test(expected = Exception.class)
    public void testCreateCardWithIncorrectBalance() {
        Card card1 = new Card("card1", -200);
    }

    @Test
    public void testGetCardBalance() {
        Card testCard1 = new Card("testCard1", 2000);
        Bank bank = new Bank();
        double expected = bank.getCardBalance(testCard1);
        Assert.assertEquals(expected, testCard1.getBalance(), 0);
    }

    @Test
    public void testAddBalance() {
        Card testCard3 = new Card("testCard3", 1000);
        Bank bank = new Bank();
        Assert.assertEquals(1000, bank.getCardBalance(testCard3), 0);
        bank.addBalance(testCard3, 200);
        Assert.assertEquals(Double.parseDouble("1200"), bank.getCardBalance(testCard3), 0);
    }

    @Test
    public void testLessBalance() {
        Card testCard4 = new Card("testCard4", 3000);
        Card testCard5 = new Card("testCard5", 5000);
        Bank bank = new Bank();
        Assert.assertEquals(3000, bank.getCardBalance(testCard4), 0);
        Assert.assertEquals(5000, bank.getCardBalance(testCard5), 0);
        bank.lessBalance(testCard4, 2000);
        bank.lessBalance(testCard5, 10000);
        Assert.assertEquals(1000, bank.getCardBalance(testCard4), 0);
        Assert.assertEquals(5000, bank.getCardBalance(testCard5), 0);
    }

    @Test
    public void testConvert() {
        Bank bank = new Bank();
        Card testCard6 = new Card("testCard6", 1000);
        Card testCard7 = new Card("testCard7", 4000);
        double expectedToDollar1 = bank.getCardBalance(testCard6) * 1.983;
        Assert.assertEquals(expectedToDollar1, bank.convert(testCard6, 1.983), 0);
        double expectedToEvro1 = bank.getCardBalance(testCard6) * 2.405;
        Assert.assertEquals(expectedToEvro1, bank.convert(testCard6, 2.405), 0);
        double expectedToDollar2 = bank.getCardBalance(testCard7) * 1.983;
        Assert.assertEquals(expectedToDollar2, bank.convert(testCard7, 1.983), 0);
        double expectedToEvro2 = bank.getCardBalance(testCard7) * 2.405;
        Assert.assertEquals(expectedToEvro2, bank.convert(testCard7, 2.405), 0);
    }

    @Test(expected = Exception.class)
    public void testIncorrectSumAddBalance() {
        Card testcard8 = new Card("testCard8", 200);
        Bank bank = new Bank();
        bank.addBalance(testcard8, -200);
    }

    @Test(expected = Exception.class)
    public void testIncorrectSumDebitCardLessBalance() {
        Card testcard8 = new Card("testCard8", 200);
        Bank bank = new Bank();
        bank.lessBalance(testcard8, -200);
    }

}
