package hw18.maksimava.Utils;

import hw18.maksimava.Browser.Browser;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Screenshot {
    protected WebDriver driver = Browser.getWebDriverInstance();

    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        return dateFormat.format(new Date()).replaceAll("\\W", "_");
    }

    public void getScreenshot() {
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenshot, new File("target/screenshots/" + "Screenshot_" + getDate() + ".png"));
        } catch (Exception ex) {
            ex.printStackTrace();

        }
    }
}
