package com.tat.maksimava.hw5;

public class Edit {
    public String[] deleteDublicates(String[] tokens) {
        for (int i = 0; i < tokens.length - 1; i++) {
            for (int j = i + 1; j < tokens.length; j++) {
                if (tokens[i].compareTo(tokens[j]) == 0) {
                    tokens[j] = " ";
                }
            }
        }
        return tokens;
    }

    public void alphabetOrder(String[] tokens) {

        for (int i = 97; i <= 122; i++) {
            char symbol = (char) i;
            String resultString = "";
            for (int j = 0; j < tokens.length; j++) {

                if (tokens[j].equals("")) {
                    j++;
                }

                if (tokens[j].charAt(0) == symbol) {
                    resultString += tokens[j] + " ";
                }
            }
            if (resultString != "") {
                showResults(symbol, resultString);
            }
        }
    }

    public void showResults(char symbol, String result) {
        System.out.println(Character.toUpperCase(symbol) + ": " + result);
    }
}
