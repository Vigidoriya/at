package hw16.maksimava.Services;

import hw16.maksimava.PageObjects.EbayResultsPage;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ResultsPageService {
    public static List<String> getTextResult() {
        List<String> textResultLinks = new ArrayList<>();
        EbayResultsPage ebayResultsPage = new EbayResultsPage();
        List<WebElement> results = ebayResultsPage.resultContent();
        Iterator<WebElement> iterator = results.iterator();
        while (iterator.hasNext()) {
            WebElement result = iterator.next();
            textResultLinks.add(result.getText().toLowerCase());
        }
        return textResultLinks;
    }

    public static void openFirstProductResult() {
        new EbayResultsPage().openTheFirstProduct();
    }
}
