package com.tat.maksimava.hw14.TUTBY.Utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.tat.maksimava.hw14.TUTBY.Browser.Browser.getWebDriverInstance;

public class Waits {
    private WebDriver driver = getWebDriverInstance();
    private int timeToWaitTitle = 5;

    public void waitForTitleContains(String title) {
        new WebDriverWait(driver, timeToWaitTitle).until(ExpectedConditions.titleContains(title));
        ;
    }
}
