package com.tat.maksimava.hw12.PageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

public class ConfigTest {
    protected WebDriver driver;
    private static final String MAIL_URL = "https://www.tut.by/";

    @BeforeClass
    public void setUp() {
        if (driver == null) {
            System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
            driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.manage().window().maximize();
            driver.get(MAIL_URL);
        }
    }

    @AfterClass
    public void reset() {
        driver.quit();
        driver = null;
    }
}
