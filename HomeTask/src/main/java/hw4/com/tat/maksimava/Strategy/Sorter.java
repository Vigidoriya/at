package hw4.com.tat.maksimava.Strategy;

interface Sorter {
    void sort(int[] array);
}