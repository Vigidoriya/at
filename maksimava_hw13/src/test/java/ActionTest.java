import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class ActionTest extends ConfigTest {
    @Test
    public void testCheckBoxRadio() {
        List<WebElement> checkboxRadio = new MainPage(driver).menuCheckboxradio("New York", "2 Star");
        Assert.assertTrue(checkboxRadio.get(0).getText().contentEquals("New York"), "Cheked " + checkboxRadio.get(0).getText());
        Assert.assertTrue(checkboxRadio.get(1).getText().contentEquals("2 Star"), "Checked " + checkboxRadio.get(1).getText());
    }

    @Test
    public void testDroppable() {
        WebElement droppable = new MainPage(driver).menuDroppable();
        Assert.assertEquals(droppable.getText(), "Dropped!", "Don't dropped");
    }


    @Test
    public void testSelectMenu() {
        List<WebElement> selectMenu = new MainPage(driver).menuSelectmenu("Fast", "3");
        Assert.assertEquals(selectMenu.get(0).getText(), "Fast", "Selected speed " + (selectMenu.get(0).getText()));
        Assert.assertEquals(selectMenu.get(1).getText(), "3", "Selected number " + selectMenu.get(1).getText());
    }

    @Test
    public void testTooltip() {
        WebElement tooltip = new MainPage(driver).menuTooltip();
        Assert.assertEquals(tooltip.getText(), "We ask for your age only for statistical purposes.", "Another text in tooltip");
    }

    @Test
    public void testTabs() {
        List<WebElement> tab = new MainPage(driver).menuTabs("Aenean lacinia");
        Assert.assertEquals(tab.get(0).getText(), "Aenean lacinia", "Incorrect tab");
        Assert.assertTrue(tab.get(1).getText().contains("Mauris eleifend est"), "Incorrect text");
    }

    @Test
    public void testSortable() {
        List<WebElement> sortable = new MainPage(driver).menuSortable("Item 7", "Item 4");
        Assert.assertEquals(sortable.get(3).getText(), "Item 7", "Item " + sortable.get(3).getText());
        Assert.assertEquals(sortable.get(4).getText(), "Item 4", "Item " + sortable.get(4).getText());
    }
}

